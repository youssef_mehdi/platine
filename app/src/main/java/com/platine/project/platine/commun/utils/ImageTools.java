package com.platine.project.platine.commun.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

/**
 * Created by you on 17/01/2017.
 */

public class ImageTools {

    //cette fonction prend une image et l'a convertit en string
    public static String bitmapToBase64(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100 , byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray,Base64.DEFAULT);
    }

    // cette fonction prend un string de base 64 et le convertit en bitmap
    public static Bitmap base64ToBitmap(String b64){
        byte[] imageAsBytes = Base64.decode(b64.getBytes() , Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes,0,imageAsBytes.length);
    }

    // changer la taille d'une image
    public static Bitmap resizeBitmap(Bitmap oldBitmap ,boolean filter){

        Bitmap newBitmap;
        // on crop l'image précédente
        if (oldBitmap.getWidth() >= oldBitmap.getHeight()){

            newBitmap = Bitmap.createBitmap(
                    oldBitmap,
                    oldBitmap.getWidth()/2 - oldBitmap.getHeight()/2,
                    0,
                    oldBitmap.getHeight(),
                    oldBitmap.getHeight()
            );

        }else{

            newBitmap = Bitmap.createBitmap(
                    oldBitmap,
                    0,
                    oldBitmap.getHeight()/2 - oldBitmap.getWidth()/2,
                    oldBitmap.getWidth(),
                    oldBitmap.getWidth()
            );
        }

        newBitmap = Bitmap.createScaledBitmap(newBitmap , 200, 200, filter);
        return newBitmap;
    }


}
