package com.platine.project.platine.login_register.view;

import android.os.Bundle;
import android.view.View;

import com.platine.project.platine.R;
import com.platine.project.platine.commun.AbstractActivity;

/**
 * Created by you on 06/01/2017.
 */

/**
 * cette classe étend la classe générique AbstractActivity
 */
public class LoginRegisterActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoginRegisterFragment mLoginRegisterFragment = new LoginRegisterFragment();

        //desactiver la toolbar
        mToolbar.setVisibility(View.GONE);

        //integrer le fragment dans l'activity
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, mLoginRegisterFragment)
                .commit();


    }


}
