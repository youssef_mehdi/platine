package com.platine.project.platine.addOffer.service;

import java.util.UUID;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by you on 11/01/2017.
 */

public interface IAddOfferWebService {

    @FormUrlEncoded
    @POST("/api/trips")
    public void createOffer(@Field("fromCity") String fromCity , @Field("toCity") String toCity , @Field("dateDepart") String date_d ,
                            @Field("dateReturn") String date_r , @Field("toAirport") String toAirport ,
                            @Field("fromAirport") String fromAirport , @Field("price") int price , @Field("priceFlight") int priceFlight,
                            @Field("priceLodging") int priceLodging , @Field("typeLodging") String lodging , @Field("nbrPlaces") int nbr_place,
                            @Field("nbrNights") int nbr_night, @Field("idUser") String uuid , Callback<String> callback);
}
