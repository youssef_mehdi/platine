package com.platine.project.platine.commun.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by you on 20/01/2017.
 */

/**
 * cette classe permet de manipuler les date : conversion en une date
 * sous la forme mm:dd:yyyy
 */
public class DateTools {


    public static String convertToAdequateDate(String d) {

        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date result1 = null;
        try {
            result1 = df1.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String format = formatter.format(result1);
        return format;
    }
}
