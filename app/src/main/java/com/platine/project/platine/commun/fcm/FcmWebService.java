package com.platine.project.platine.commun.fcm;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by you on 18/01/2017.
 */

public interface FcmWebService {

    @FormUrlEncoded
    @POST("/api/users/updateToken")
    public void sendRegistrationToServer(@Field("id") String id_user,@Field("token") String idRegistration
            , Callback<String> callback);
}
