package com.platine.project.platine.myTrips.requests.presenter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.platine.project.platine.R;
import com.platine.project.platine.addOffer.module.Trips;
import com.platine.project.platine.commun.utils.DateTools;
import com.platine.project.platine.commun.utils.ImageConverterCircle;
import com.platine.project.platine.commun.utils.ImageTools;
import com.platine.project.platine.details_offer.view.DetailsOfferActivity;
import com.platine.project.platine.login.model.User;
import com.platine.project.platine.myTrips.requests.model.ResponseRequest;
import com.platine.project.platine.myTrips.requests.view.IRequestService;
import com.platine.project.platine.myTrips.requests.view.RequestsActivity;
import com.platine.project.platine.myTrips.view.MyTripsActivity;
import com.platine.project.platine.profil.view.ProfilActivity;

import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by you on 21/01/2017.
 */

public class TripRequestAdapter extends RecyclerView.Adapter<TripRequestAdapter.MyViewHolder>  {

    private List<ResponseRequest> responseRequests;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // récuperation des composants utilisé pour chaque item de la recyclerview
        @BindView(R.id.request_userName)
        TextView request_userName; // le nom de l'utilisateur demandant de rejoindre le trip
        @BindView(R.id.request_thumbnail)
        ImageView request_thumbnail; // l'image de l'utilisateur demandant de rejoindre le trip
        @BindView(R.id.request_addUser)
        ImageView request_addUser; // bouton pour ajouter la demande
        @BindView(R.id.request_deleteUser)
        ImageView request_deleteUser; // bouton pour supprimer la demande

        public MyViewHolder(final View view) {
            super(view);
            ButterKnife.bind(this, view);
            //on rajoute des listener sur les deux images view : ajouter et supprimer
            request_addUser.setOnClickListener(this);
            request_deleteUser.setOnClickListener(this);
            request_thumbnail.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            RequestPresenter requestPresenter = new RequestPresenter();
            if (v.getId() == request_addUser.getId()) {
                requestPresenter.confirmationAccepted(responseRequests.get(position).getId_participation(), 1);
                Intent intent = new Intent(mContext , MyTripsActivity.class);
                mContext.startActivity(intent);
            }
            else if (v.getId() == request_deleteUser.getId()) {
                requestPresenter.confirmationAccepted(responseRequests.get(position).getId_participation(), -1);
                Intent intent = new Intent(mContext , MyTripsActivity.class);
                mContext.startActivity(intent);
            }
            else if(v.getId() == request_thumbnail.getId()){
                Intent intent = new Intent(mContext, ProfilActivity.class);
                intent.putExtra("idUser" , responseRequests.get(position).getId_user());
                mContext.startActivity(intent);
            }
        }
    }


    public TripRequestAdapter(List<ResponseRequest> responseRequests , Context ctx) {
        this.responseRequests = responseRequests;
        mContext = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_requests_row , parent, false);

        return new MyViewHolder(itemView);
    }

    // remplir les champs d'un item de la recyclerView
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ResponseRequest responseRequest = responseRequests.get(position);
        if(responseRequest.getConfirmed() != 0 ){
            holder.request_addUser.setVisibility(View.GONE);
            holder.request_deleteUser.setVisibility(View.GONE);
        }
        holder.request_userName.setText(responseRequest.getName_user());
        if( responseRequest.getThumbnail() != null) {
            Bitmap thumbnail = ImageTools.base64ToBitmap(responseRequest.getThumbnail());
            holder.request_thumbnail.setImageBitmap(ImageConverterCircle.getCircleBitmap(thumbnail));
        }
    }

    @Override
    public int getItemCount() {
        return responseRequests.size();
    }
}

