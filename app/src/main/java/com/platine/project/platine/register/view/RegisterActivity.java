package com.platine.project.platine.register.view;

import android.os.Bundle;
import android.view.View;

import com.platine.project.platine.R;
import com.platine.project.platine.commun.AbstractActivity;

/**
 * Created by you on 06/01/2017.
 */

public class RegisterActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //desactiver la toolbar
        mToolbar.setVisibility(View.GONE);

        RegisterFragment mRegisterFragment = new RegisterFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, mRegisterFragment)
                .commit();

    }

}
