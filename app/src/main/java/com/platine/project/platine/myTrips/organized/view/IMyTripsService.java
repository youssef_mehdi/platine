package com.platine.project.platine.myTrips.organized.view;

import com.platine.project.platine.addOffer.module.Trips;

import java.util.List;

/**
 * Created by you on 20/01/2017.
 */

public interface IMyTripsService {

    public void listTrips(List<Trips> listTrip);
    public void loadingShow(boolean bool);
    public void loadingError(String msg);
    public void list_offer_empty();
}
