package com.platine.project.platine.details_offer.view;

/**
 * Created by you on 09/01/2017.
 */

public interface IDetailsOfferService {

    public void sendJoinRequest();
    public void requestParticipationSendSuccessfully(String msg);
    public void requestParticipationSendFailure(String msg);
    public void serverError(String msg);
}
