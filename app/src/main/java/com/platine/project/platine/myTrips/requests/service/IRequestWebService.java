package com.platine.project.platine.myTrips.requests.service;

import com.platine.project.platine.login.model.User;
import com.platine.project.platine.myTrips.requests.model.ResponseRequest;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by you on 22/01/2017.
 */

public interface IRequestWebService {

    /**
     * renvoie la liste des users qui ont demandé de participer au trip id_trip
     * @param id_trip  l'id du trip
     * @param callback liste des users
     */
    @GET("/api/participation/getUsersByTripId/{id}")
    public void getUsersByTripId(@Path("id") String id_trip , Callback<List<ResponseRequest>> callback);

    /**
     *
     * @param id l'id de participation
     * @param confirmed boolean 1 = accepté , -1 = refuser
     * @param callback "1"= l'appel s'est bien effectué ou "-1"
     */
    @FormUrlEncoded
    @POST("/api/participation/confirm")
    public void confirmedRequest(@Field("idParticipation") String id , @Field("confirmed") int confirmed ,
                                 Callback<String> callback);


    /**
     * renvoie la liste des utilisateurs acceptés pour cet idTrip
     * @param idTrip
     * @param callback
     */
    @GET("/api/participation/getUsersAcceptedByTripId/{id}")
    public void getUsersAcceptedByTripId(@Path("id") String idTrip , Callback<List<ResponseRequest>> callback);
}
