package com.platine.project.platine.myTrips.requests.model;

import com.platine.project.platine.login.model.User;

/**
 * Created by you on 22/01/2017.
 */

public class ResponseRequest {

    private String id_user;
    private String name_user;
    private String thumbnail;
    private String id_participation;
    private int confirmed; // 1 = confirmé , 0 = demande en cours , -1 = refuser

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getName_user() {
        return name_user;
    }

    public void setName_user(String name_user) {
        this.name_user = name_user;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getId_participation() {
        return id_participation;
    }

    public void setId_participation(String id_participation) {
        this.id_participation = id_participation;
    }

    public int getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(int confirmed) {
        this.confirmed = confirmed;
    }
}
