package com.platine.project.platine.home.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.platine.project.platine.commun.utils.WebServiceTools;
import com.platine.project.platine.home.service.IHomeWebService;
import com.platine.project.platine.home.view.IHomeService;
import com.platine.project.platine.addOffer.module.Trips;
import com.platine.project.platine.login.model.User;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by you on 08/01/2017.
 */

public class HomePresenter extends WebServiceTools {

    IHomeWebService iHomeWebService = new RestAdapter.Builder()
            .setEndpoint(WEBSERVICELINK)
            .setLogLevel(RestAdapter.LogLevel.FULL)
            .build()
            .create(IHomeWebService.class);

    IHomeService mIHomeService;

    public HomePresenter(IHomeService iHomeService){
        mIHomeService = iHomeService;
    }

    // recupere toutes les offres
    public void getAllTrips(){
        mIHomeService.loadingShow(true);
        iHomeWebService.getAllTrips(new Callback<List<Trips>>() {
            @Override
            public void success(List<Trips> trips, Response response) {
                mIHomeService.loadingShow(false);
                mIHomeService.listTrips(trips);
                if ( trips.size() == 0){
                    mIHomeService.list_offer_empty("Aucune offre ne correpond à votre recherche");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                mIHomeService.loadingShow(false);
                mIHomeService.loadingError("Erreur de chargement, réessayez");
            }
        });
    }

    // fait appel au web service pour récuperer les offres qui correspondent au données rentrées dans le formulaire
    public void getTripsFindForms( String fromCity , String toCity , String dateBegin , String dateReturn,int priceMax , int priceMin) {
        if ( fromCity.equals(""))
            fromCity = "0";
        if(toCity.equals(""))
            toCity = "0";
        if(dateBegin.equals(""))
            dateBegin = "0";
        if (dateReturn.equals(""))
            dateReturn = "0";

        mIHomeService.loadingShow(true);
        iHomeWebService.getAllTripsFindByForm(fromCity, toCity, dateBegin, dateReturn, priceMax, priceMin, new Callback<List<Trips>>() {

            @Override
            public void success(List<Trips> trips, Response response) {
                mIHomeService.loadingShow(false);
                if(trips.size() == 0){
                    mIHomeService.list_offer_empty("Aucune offre à afficher");
                }
                else {
                    mIHomeService.listTrips(trips);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                mIHomeService.loadingShow(false);
                mIHomeService.loadingError("Une erreur est survenu lors du chargement des offres");

            }
        });
    }

}
