package com.platine.project.platine.addOffer.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.platine.project.platine.R;
import com.platine.project.platine.addOffer.presenter.AddOfferPresenter;
import com.platine.project.platine.commun.auto_complete_text.ServicePlaceGoogle;
import com.platine.project.platine.commun.datePicker.DatePickerService;
import com.platine.project.platine.home.view.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by you on 11/01/2017.
 */

public class AddOfferFragment extends Fragment implements IAddOfferService {

    @BindView(R.id.messageError)
    TextView messageError;
    @BindView(R.id.auto_complete_offre_depart_)
    EditText city_begin; // ville de depart
    @BindView(R.id.auto_complete_offre_destination)
    EditText city_destination; // ville de destination
    @BindView(R.id.offre_date_begin)
    EditText date_begin; // date de depart
    @BindView(R.id.offre_date_return)
    EditText date_return; // date de retour
    @BindView(R.id.auto_complete_offre_airport_begin)
    EditText airport_begin; // aeroport de depart
    @BindView(R.id.auto_complete_offre_airport_destination)
    EditText airport_destination; // aeroport de destination
    @BindView(R.id.offer_price)
    EditText offer_price; // prix de l'offre
    @BindView(R.id.offre_price_flight)
    EditText offre_price_flight; // prix du vol
    @BindView(R.id.spinner_type)
    Spinner type_lodgment; // type  de logement : hotel , auberge , appartement
    @BindView(R.id.offre_number_place)
    EditText number_place; //  nombre de place total
    @BindView(R.id.offre_price_per_person)
    EditText price_lodging_per_person; // prix par personne
    @BindView(R.id.offre_number_night)
    EditText number_night; // nombre de nuit
    @BindView(R.id.progressBarAddOffer)
    ProgressBar progressBar; //le loader

    AddOfferPresenter mAddOfferPresenter; // le presenter
    DatePickerService mDatePickerService; // date picker
    ServicePlaceGoogle mServicePlaceGoogle; // api google places

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // On récupère la view du fragment
        View view = inflater.inflate(R.layout.activity_add_offer, container, false);
        // On bind la view du fragment pour l'utiliser avec ButterKnife.
        ButterKnife.bind(this, view);
        // On initialise notre presenter
        mAddOfferPresenter = new AddOfferPresenter(this, getContext());
        // initialisation des dates picker
        mDatePickerService = new DatePickerService(getActivity(), date_begin, date_return);
        // initialisation des champs google places
        //mServicePlaceGoogle = new ServicePlaceGoogle(getActivity(), city_begin, city_destination, airport_begin, airport_destination);

        return view;
    }

    // fait appel au web service

    /**
     *
     */
    @OnClick(R.id.but_submit_offer)
    @Override
    public void createOffer() {

        if ( validateInputFields(city_begin.getText().toString(), city_destination.getText().toString(), date_begin.getText().toString()
                , date_return.getText().toString(), airport_destination.getText().toString()
                , airport_begin.getText().toString(),offer_price.getText().toString(),
                "" + offre_price_flight.getText().toString(), "" + price_lodging_per_person.getText().toString()
                , "" + number_place.getText().toString(), "" + number_night.getText().toString()) )

        {
            mAddOfferPresenter.createOffer(city_begin.getText().toString(), city_destination.getText().toString(),
                    date_begin.getText().toString(), date_return.getText().toString(),
                    airport_destination.getText().toString(), airport_begin.getText().toString(),
                    Integer.valueOf(offer_price.getText().toString()), Integer.valueOf(offre_price_flight.getText().toString()),
                    Integer.valueOf(price_lodging_per_person.getText().toString()), (String) type_lodgment.getSelectedItem(),
                    Integer.valueOf(number_place.getText().toString()),
                    Integer.valueOf(number_night.getText().toString()));
        }

        else{
            messageError.setVisibility(View.VISIBLE);
        }


    }

    //cette fonction verifie si tout les champs sont remplis
    private boolean validateInputFields(String fromCity, String toCity, String date_d, String date_r,
                              String toAirport, String fromAirport,String priceOffer, String priceFlight, String priceLodgment,
                              String nbr_place, String nbr_night) {

        if (fromCity.length() == 0 || toCity.length() == 0 || date_d.length() == 0 || date_r.length() == 0 ||
                fromAirport.length() == 0 || toAirport.length() == 0 || priceOffer.length() == 0 ||
                priceFlight.length() == 0 || nbr_place.length() == 0 || priceLodgment.length() == 0 || nbr_night.length() == 0 )
        {
            return false;
        }
        return true;
    }

    @Override
    public void showLoading(boolean bool) {
        if (bool) {
            progressBar.setVisibility(View.VISIBLE);
        } else
            progressBar.setVisibility(View.GONE);
    }

    // si l'ajout de l'offre s'est bien déroulé
    @Override
    public void addOfferSuccess(String msg) {
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);
    }

    //si l'ajout de l'offre a rencontré un problème
    @Override
    public void addOfferFailed(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void fieldFromCityIsEmpty(String msgErr) {
        city_begin.setError(msgErr);
    }

    @Override
    public void fieldToCityIsEmpty(String msgErr) {
        city_destination.setError(msgErr);
    }

    @Override
    public void fieldDateDebutIsEmpty(String msgErr) {
        date_begin.setError(msgErr);
    }

    @Override
    public void fieldDateReturnIsEmpty(String msgErr) {
        date_return.setError(msgErr);
    }

    @Override
    public void fieldFromAirportIsEmpty(String msgErr) {
        airport_begin.setError(msgErr);
    }

    public void fieldPriceOfferIsEmpty(String msgErr){ offer_price.setError(msgErr);}

    @Override
    public void fieldToAirportIsEmpty(String msgErr) {
        airport_destination.setError(msgErr);
    }

    @Override
    public void fieldPriceFlightIsEmpty(String msgErr) {
        offer_price.setError(msgErr);
    }

    @Override
    public void fieldNbrPlaceIsEmpty(String msgErr) {
        number_place.setError(msgErr);
    }

    @Override
    public void fieldPriceLodgmentIsEmpty(String msgError) {
        price_lodging_per_person.setError(msgError);
    }

    @Override
    public void fieldNbrNightIsEmpty(String msgErr) {
        number_night.setError(msgErr);
    }

}
