package com.platine.project.platine.addOffer.presenter;

import android.content.Context;

import com.platine.project.platine.addOffer.service.IAddOfferWebService;
import com.platine.project.platine.addOffer.view.IAddOfferService;
import com.platine.project.platine.commun.utils.MySharedPreferences;
import com.platine.project.platine.commun.utils.WebServiceTools;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by you on 11/01/2017.
 */

public class AddOfferPresenter extends WebServiceTools {

    IAddOfferService mIAddOfferService;
    Context mContext;

    IAddOfferWebService mWebService = new RestAdapter.Builder()
            .setEndpoint(WEBSERVICELINK)
            .setLogLevel(RestAdapter.LogLevel.FULL)
            .build()
            .create(IAddOfferWebService.class);

    public AddOfferPresenter(IAddOfferService iAddOfferWebService, Context ctx) {
        mIAddOfferService = iAddOfferWebService;
        mContext = ctx;
    }

    //cette fonction envoie l'offre au web service par la methode POST
    public void createOffer(String fromCity, String toCity, String date_d, String date_r,
                            String toAirport, String fromAirport, int priceOffer, int priceFlight, int priceLodging, String lodging,
                            int nbr_place, int nbr_night) {

        String id = MySharedPreferences.getStoredId(mContext);
        mIAddOfferService.showLoading(true);
        mWebService.createOffer(fromCity, toCity, date_d, date_r, toAirport, fromAirport,
                priceOffer, priceFlight, priceLodging, lodging, nbr_place, nbr_night, id, new Callback<String>() {

                    @Override
                    public void success(String i, Response response) {
                        mIAddOfferService.showLoading(false);
                        if (i.equals("1")) {
                            mIAddOfferService.addOfferSuccess("l'offre a bien été ajoutée");
                        }
                        else{
                            mIAddOfferService.addOfferFailed("l'offre n'a pas pu être ajoutée");
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        mIAddOfferService.showLoading(false);
                        mIAddOfferService.addOfferFailed("l'offre n'a pas pu être ajoutée");
                    }
                });
    }


}
