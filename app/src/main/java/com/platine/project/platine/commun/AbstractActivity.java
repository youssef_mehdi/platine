package com.platine.project.platine.commun;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.platine.project.platine.R;
import com.platine.project.platine.myTrips.view.MyTripsActivity;
import com.platine.project.platine.profil.view.ProfilActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by you on 05/01/2017.
 */

/**
 * le super activity, toutes les autres activity hérite de celle la
 */
public abstract class AbstractActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    public Toolbar mToolbar;
    @BindView(R.id.tab_layout)
    public TabLayout tabLayout;
    @BindView(R.id.pager)
    public ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_generic_2);

        ButterKnife.bind(this);
        ButterKnife.bind(mToolbar);

        /**
         * changer la couleur de la status bar pour les versions récentes d'android
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.statusBarColor));
        }
    }

    /**
     * redirection vers le profil quand on clique sur l'icone du profil
     */
    @OnClick(R.id.account_item)
    public void goToUserAccount() {
        Intent intent = new Intent(this, ProfilActivity.class);
        startActivity(intent);
    }

    /**
     * redirection vers l'activity mes voyages quand on clique sur l'icone list de la toolbar
     */
    @OnClick(R.id.myListTrips)
    public void goToMyTrips() {
        Intent intent = new Intent(this, MyTripsActivity.class);
        startActivity(intent);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
