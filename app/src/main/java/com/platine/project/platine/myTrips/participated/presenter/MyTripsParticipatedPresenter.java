package com.platine.project.platine.myTrips.participated.presenter;

import com.platine.project.platine.addOffer.module.Trips;
import com.platine.project.platine.commun.utils.MySharedPreferences;
import com.platine.project.platine.commun.utils.WebServiceTools;
import com.platine.project.platine.myTrips.participated.service.IParticipatedWebService;
import com.platine.project.platine.myTrips.participated.view.IParticipatedService;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by you on 23/01/2017.
 */

public class MyTripsParticipatedPresenter extends WebServiceTools{

    IParticipatedService mIParticipatedService;

    IParticipatedWebService mIparticipatedWebService = new RestAdapter.Builder()
            .setEndpoint(WEBSERVICELINK)
    .setLogLevel(RestAdapter.LogLevel.FULL)
    .build()
    .create(IParticipatedWebService.class);

    public MyTripsParticipatedPresenter(IParticipatedService iParticipatedService){
        mIParticipatedService = iParticipatedService;
    }


    public void getTripsAcceptedById(String idUser){
        mIParticipatedService.loadingShow(true);
        mIparticipatedWebService.getTripsParticipatedByUserId(idUser, new Callback<List<Trips>>() {
            @Override
            public void success(List<Trips> trips, Response response) {
                mIParticipatedService.loadingShow(false);
                mIParticipatedService.listTrips(trips);
                if ( trips.size() == 0){
                    mIParticipatedService.list_offer_empty();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                mIParticipatedService.loadingShow(false);
                mIParticipatedService.loadingError("Erreur de chargement, réessayez");
            }
        });
    }

}
