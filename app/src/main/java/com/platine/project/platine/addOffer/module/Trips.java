package com.platine.project.platine.addOffer.module;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by you on 08/01/2017.
 */

public class Trips implements Serializable {


    private UUID id_trip;
    private UUID id_user; //proprietaire de l'offre
    private String name_user; // le nom du proprietaire
    private String thumbnail; // l'image recardré qui va etre afficher dans la recyclerView
    private String from_city; // ville de depart
    private String to_city; // ville d'arrivé
    private String date_d; // date de depart du voyage
    private String date_a; // date arrivé du voyage
    private Integer price; // le prix du voyage
    private Integer price_flight; // le prix du billet d'avion
    private Integer price_lodging; // le prix d'hebergement
    private String departure; // aeroport de depart
    private String arrival; // aeroport d'arrivé
    private String date_d_flight; // la date de depart du vol
    private String date_a_flight; // la date d'arrivé du vol
    private String lodging; // type de logement : hotel , auberge , appartement
    private Integer nbr_places; // le nombre de place du voyage
    private Integer nbr_participants; // le nombre de participant
    private Integer nbr_nights; // le nombre de nuit à lhotel par exemple

    public UUID getId_trip() {
        return id_trip;
    }

    public void setId_trip(UUID id_trip) {
        this.id_trip = id_trip;
    }

    public String getFrom_city() {
        return from_city;
    }

    public void setFrom_city(String from_city) {
        this.from_city = from_city;
    }

    public String getName_user() {
        return name_user;
    }

    public void setName_user(String name_user) {
        this.name_user = name_user;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getTo_city() {
        return to_city;
    }

    public void setTo_city(String to_city) {
        this.to_city = to_city;
    }

    public String getDate_d() {
        return date_d;
    }

    public void setDate_d(String date_d) {
        this.date_d = date_d;
    }

    public String getDate_a() {
        return date_a;
    }

    public void setDate_a(String date_a) {
        this.date_a = date_a;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getLodging() {
        return lodging;
    }

    public void setLodging(String lodging) {
        this.lodging = lodging;
    }

    public Integer getNbr_places() {
        return nbr_places;
    }

    public void setNbr_places(Integer nbr_places) {
        this.nbr_places = nbr_places;
    }

    public Integer getNbr_participants() {
        return nbr_participants;
    }

    public void setNbr_participants(Integer nbr_participants) {
        this.nbr_participants = nbr_participants;
    }

    public UUID getId_user(){
        return this.id_user;
    }

    public void setId_user(UUID id_user) {
        this.id_user = id_user;
    }

    public Integer getPrice_flight() {
        return price_flight;
    }

    public void setPrice_flight(Integer price_flight) {
        this.price_flight = price_flight;
    }

    public Integer getPrice_lodging() {
        return price_lodging;
    }

    public void setPrice_lodging(Integer price_lodging) {
        this.price_lodging = price_lodging;
    }

    public String getDate_d_flight() {
        return date_d_flight;
    }

    public void setDate_d_flight(String date_d_flight) {
        this.date_d_flight = date_d_flight;
    }

    public String getDate_a_flight() {
        return date_a_flight;
    }

    public void setDate_a_flight(String date_a_flight) {
        this.date_a_flight = date_a_flight;
    }

    public Integer getNbr_nights() {
        return nbr_nights;
    }

    public void setNbr_nights(Integer nbr_nights) {
        this.nbr_nights = nbr_nights;
    }




    @Override
    public String toString() {
        return "Trips{" +
                "id_trip=" + id_trip +
                ",name_user=" + name_user+
                ", from_city='" + from_city + '\'' +
                ", to_city='" + to_city + '\'' +
                ", date_d='" + date_d + '\'' +
                ", date_a='" + date_a + '\'' +
                '}';
    }
}
