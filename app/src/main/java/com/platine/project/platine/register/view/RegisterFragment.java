package com.platine.project.platine.register.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.platine.project.platine.R;
import com.platine.project.platine.commun.auto_complete_text.ServicePlaceGoogle;
import com.platine.project.platine.commun.datePicker.DatePickerService;
import com.platine.project.platine.home.view.HomeActivity;
import com.platine.project.platine.register.presenter.RegisterPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by you on 11/01/2017.
 */


public class RegisterFragment extends Fragment implements IRegisterService {

    @BindView(R.id.msgError)
    TextView msgError;
    @BindView(R.id.register_phone)
    EditText register_phone;
    @BindView(R.id.date_birth)
    TextInputEditText dateBirth; // date de naissance
    @BindView(R.id.autoCompleteTextViewCity)
    EditText autoCompleteTextViewCity; // ville
    @BindView(R.id.edit_text_name)
    TextInputEditText name; // nom de l'utilisateur
    @BindView(R.id.edit_text_email)
    TextInputEditText email; // l'email de l'utilisateur
    @BindView(R.id.checkBox_man)
    CheckBox checkBoxMan; // check box homme
    @BindView(R.id.checkBox_women)
    CheckBox checkBoxWomen; // check box femme
    @BindView(R.id.edit_text_password)
    TextInputEditText password; // le mot de passe
    @BindView(R.id.edit_text_password_confirmed)
    TextInputEditText passwordConfirm; // le mot de passe de confirmation
    @BindView(R.id.progressBar)
    ProgressBar progressBar; // le loader

    RegisterPresenter registerPresenter; // le presenter de cette activity
    DatePickerService mDatePickerService; // le calendrier
    ServicePlaceGoogle mServicePlaceGoogle; // api google places

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // On récupère la view du fragment
        View view = inflater.inflate(R.layout.activity_register_fragment, container, false);

        // On bind la view du fragment pour l'utiliser avec ButterKnife.
        ButterKnife.bind(this, view);

        // On initialise notre presenter
        registerPresenter = new RegisterPresenter(getContext(), this);
        mDatePickerService = new DatePickerService(getActivity(), dateBirth);
        //mServicePlaceGoogle = new ServicePlaceGoogle(getActivity(), autoCompleteTextViewCity, null);

        return view;
    }

    // lorsqu'on clique sur le bouton s'enregistrer , on envoie
    // les informations du formulaire au serveur pour créer un utilisateur
    @OnClick(R.id.but_register)
    @Override
    public void createUser() {

        if(dateBirth.getText().toString().length() == 0 || autoCompleteTextViewCity.getText().toString().length() == 0 ||
                name.getText().toString().length() == 0 || email.getText().toString().length() == 0 ||
                register_phone.getText().toString().length() == 0 )
        {
            msgError.setVisibility(View.VISIBLE);
        }
        else{
            registerPresenter.register(name.getText().toString(), email.getText().toString(),
                    dateBirth.getText().toString(), autoCompleteTextViewCity.getText().toString(),
                    checkBoxMan.isChecked() ? "m" : "f", register_phone.getText().toString(),
                    password.getText().toString(), passwordConfirm.getText().toString());
        }

    }

    @Override
    public void emailInvalid(String msg) {
        email.setError(msg);
    }


    @Override
    public void passwordInvalid(String msg) {
        password.setError(msg);
    }

    @Override
    public void confirmPasswordInvalid(String msg) {
        passwordConfirm.setError(msg);
    }

    @Override
    public void showLoading(boolean bool) {
        if (bool) {
            progressBar.setVisibility(View.VISIBLE);
        } else
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public void registrationSuccess() {
        // on met a jour le token
        registerPresenter.fcmRegistration();
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);
    }

    @Override
    public void registrationFailed(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setPositiveButton("Réessayez", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
