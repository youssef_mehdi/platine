package com.platine.project.platine.details_offer.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.platine.project.platine.R;
import com.platine.project.platine.addOffer.module.Trips;
import com.platine.project.platine.commun.utils.DateTools;
import com.platine.project.platine.commun.utils.MySharedPreferences;
import com.platine.project.platine.details_offer.presenter.DetailsOfferPresenter;
import com.platine.project.platine.home.view.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by you on 09/01/2017.
 */

public class DetailsOfferFragment extends Fragment implements IDetailsOfferService {

    // les informations d'une offre
    @BindView(R.id.detail_airport_begin)
    TextView detail_airport_begin;
    @BindView(R.id.detail_airport_destination)
    TextView detail_airport_destination;
    @BindView(R.id.detail_date_begin)
    TextView detail_date_begin;
    @BindView(R.id.detail_date_return)
    TextView detail_date_return;
    @BindView(R.id.detail_nbr_night)
    TextView detail_nbr_night;
    @BindView(R.id.detail_nbr_place)
    TextView detail_nbr_place;
    @BindView(R.id.detail_nbr_place_free)
    TextView detail_nbr_place_free;
    @BindView(R.id.detail_price)
    TextView detail_price;
    @BindView(R.id.detail_price_per_person)
    TextView detail_price_per_person;
    @BindView(R.id.detail_type)
    TextView detail_type;
    @BindView(R.id.detail_city_destination)
    TextView detail_city_destination;
    @BindView(R.id.detail_city_begin)
    TextView detail_city_begin;
    @BindView(R.id.but_join)
    Button but_join;

    private DetailsOfferPresenter mDetailsOfferPresenter;
    private Trips tripSelected; // le trip séléctionnée dans l'accueil

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // On récupère la view du fragment
        View view = inflater.inflate(R.layout.activity_details_offre_fragment, container, false);

        // On bind la view du fragment pour l'utiliser avec ButterKnife.
        ButterKnife.bind(this, view);

        // On initialise notre presenter
        mDetailsOfferPresenter = new DetailsOfferPresenter(this, getContext());

        tripSelected = (Trips) getActivity().getIntent().getSerializableExtra("tripSelected");
        // remplir les champs du details de l'offre selectionné
        fillForm(tripSelected);

        return view;
    }

    /**
     * remplit les champs d'un trip
     *
     * @param tripSelected le trip selectionné à l'accueil
     */
    public void fillForm(Trips tripSelected) {

        detail_airport_begin.setText(tripSelected.getDeparture());
        detail_airport_destination.setText(tripSelected.getArrival());
        detail_date_begin.setText(DateTools.convertToAdequateDate(tripSelected.getDate_d()));
        detail_date_return.setText(DateTools.convertToAdequateDate(tripSelected.getDate_a()));
        detail_nbr_night.setText("" + tripSelected.getNbr_nights());
        detail_nbr_place.setText(String.valueOf(tripSelected.getNbr_places()));
        detail_nbr_place_free.setText(String.valueOf(tripSelected.getNbr_places()));
        detail_price.setText(String.valueOf(tripSelected.getPrice()));
        detail_price_per_person.setText("" + tripSelected.getPrice_lodging());
        detail_type.setText(tripSelected.getLodging());
        detail_city_destination.setText(tripSelected.getTo_city());
        detail_city_begin.setText(tripSelected.getFrom_city());
    }

    /**
     * lorsqu'on clique sur le bouton rejoindre
     */
    @OnClick(R.id.but_join)
    @Override
    public void sendJoinRequest(){
        if (!(MySharedPreferences.getStoredId(getContext()).equals(""+tripSelected.getId_user()))) {
            mDetailsOfferPresenter.sendRequest(tripSelected.getId_trip());
        }
        else{
            Toast.makeText(getContext() , "vous êtes propriètaire de l'annonce" , Toast.LENGTH_LONG).show();
        }
    }

    /**
     * affichage d'une alerte lorsqu'on clique sur le bouton rejoindre
     *
     * @param msg message affiché sur l'alerte dialog
     */
    @Override
    public void requestParticipationSendSuccessfully(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setPositiveButton("Fermer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Affichage d'une alert dialog lorsque l'utilisateur veut rejoindre le même voyage deux fois
     * @param msg message à afficher dans l'alerte
     */
    @Override
    public void requestParticipationSendFailure(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setPositiveButton("Fermer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    public void serverError(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setPositiveButton("Fermer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(getActivity() , HomeActivity.class);
                startActivity(intent);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
