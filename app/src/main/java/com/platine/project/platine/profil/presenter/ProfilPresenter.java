package com.platine.project.platine.profil.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;

import com.platine.project.platine.commun.utils.ImageConverterCircle;
import com.platine.project.platine.commun.utils.ImageTools;
import com.platine.project.platine.commun.utils.MySharedPreferences;
import com.platine.project.platine.commun.utils.WebServiceTools;
import com.platine.project.platine.login.model.User;
import com.platine.project.platine.profil.service.IProfilWebService;
import com.platine.project.platine.profil.view.IProfilService;

import java.util.List;
import java.util.UUID;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by you on 12/01/2017.
 */

public class ProfilPresenter extends WebServiceTools {

    private IProfilService mIProfilService;
    private Context mContext;
    private String id ;

    IProfilWebService iProfilWebService = new RestAdapter.Builder()
            .setEndpoint(WEBSERVICELINK)
            .setLogLevel(RestAdapter.LogLevel.FULL)
            .build()
            .create(IProfilWebService .class);

    public ProfilPresenter(IProfilService iProfilService , Context ctx){
        mIProfilService = iProfilService;
        mContext = ctx;
        id =  MySharedPreferences.getStoredId(mContext);
    }


    public void updateImage( Bitmap imageCircle , Bitmap imageOriginal, String email , String name ,
                             String city , String birthday, String phone){
        String thumbnailToString = ImageTools.bitmapToBase64(imageOriginal);
        iProfilWebService.updateUser(id, "", thumbnailToString,email, name, city, birthday,phone, new Callback<String>() {
            @Override
            public void success(String call, Response response) {
                if(call.equals("1")){
                    mIProfilService.imageUploadSuccess("Vos informations ont bien été enregistrée!");
                }
            }
            @Override
            public void failure(RetrofitError error) {
                mIProfilService.imageUploadFailure("Vos informations n'ont pas été enregistrée");
            }
        });
    }

    public void getUser(String idUser){
        mIProfilService.showLoading(true);
        iProfilWebService.getUser(idUser, new Callback<List<User>>() {
            @Override
            public void success(List<User> users, Response response) {
                mIProfilService.showLoading(false);
                mIProfilService.populateFields(users.get(0));
            }

            @Override
            public void failure(RetrofitError error) {
                mIProfilService.showLoading(false);
                mIProfilService.failureLoadingUser("Erreur lors du chargement des informations de l'utilisateur");
            }
        });
    }
}
