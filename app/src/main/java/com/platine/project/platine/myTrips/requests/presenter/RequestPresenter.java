package com.platine.project.platine.myTrips.requests.presenter;

import android.util.Log;
import android.widget.Toast;

import com.platine.project.platine.commun.utils.WebServiceTools;
import com.platine.project.platine.login.model.User;
import com.platine.project.platine.myTrips.requests.model.ResponseRequest;
import com.platine.project.platine.myTrips.requests.service.IRequestWebService;
import com.platine.project.platine.myTrips.requests.view.IRequestService;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by you on 22/01/2017.
 */

public class RequestPresenter extends WebServiceTools {

    // le service de la vue
    IRequestService mIRequestService;

    // web service
    IRequestWebService mIRequestWebService = new RestAdapter.Builder()
            .setEndpoint(WEBSERVICELINK)
            .setLogLevel(RestAdapter.LogLevel.FULL)
            .build()
            .create(IRequestWebService.class);

    public RequestPresenter(IRequestService mIRequestService) {
        this.mIRequestService = mIRequestService;
    }

    public RequestPresenter(){

    }

    /**
     * reçoit de l'appel web service la liste des utilisateurs qui ont rejoint le trip id_trip
     * @param id_trip
     */
    public void getRequest(String id_trip){
        mIRequestService.showLoading(true);
        mIRequestWebService.getUsersByTripId(id_trip, new Callback<List<ResponseRequest>>(){
            @Override
            public void success(List<ResponseRequest> responseRequests, Response response) {
                mIRequestService.showLoading(false);
                mIRequestService.listUsers(responseRequests);
                if(responseRequests.size() == 0 ){
                    mIRequestService.list_request_empty();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                mIRequestService.showLoading(false);

            }
        });
    }

    /**
     *
     * @param idParticipation
     * @param confirm boolean 1 = accepter a rejoindre le trip , 0 = refuser à rejoindre le trip
     */
    public void confirmationAccepted(String idParticipation , int confirm){
        mIRequestWebService.confirmedRequest(idParticipation, confirm, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                if(s.equals("1")){
                }
                else{
                }
            }

            @Override
            public void failure(RetrofitError error) {
                //mIRequestService.errorLoading();
            }
        });
    }

    public void getUsersAccepted(String idTrip){
        mIRequestService.showLoading(true);
        mIRequestWebService.getUsersAcceptedByTripId(idTrip, new Callback<List<ResponseRequest>>() {
            @Override
            public void success(List<ResponseRequest> responseRequests, Response response) {
                mIRequestService.showLoading(false);
                mIRequestService.listUsers(responseRequests);
                if(responseRequests.size() == 0 ){
                    mIRequestService.list_request_empty();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                mIRequestService.showLoading(false);
            }
        });

    }
}
