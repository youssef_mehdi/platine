package com.platine.project.platine.details_offer.view;

import android.os.Bundle;
import android.widget.TextView;

import com.platine.project.platine.R;
import com.platine.project.platine.commun.AbstractActivity;

import butterknife.BindView;

/**
 * Created by you on 09/01/2017.
 */

public class DetailsOfferActivity extends AbstractActivity {

    @BindView(R.id.app_name_text)
    TextView app_name_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app_name_text.setText("Détails de l'offre");

        DetailsOfferFragment mDetailsOfferFragment = new DetailsOfferFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, mDetailsOfferFragment)
                .commit();

        // la fleche pour revenir à l'activity parente : accueil
        setSupportActionBar(mToolbar);
        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

    }

}
