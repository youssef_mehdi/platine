package com.platine.project.platine.profil.service;

import com.platine.project.platine.login.model.User;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by you on 12/01/2017.
 */

public interface IProfilWebService {

    @GET("/api/users/{id}")
    public void getUser(@Path("id") String id , Callback<List<User>> callback);

    @FormUrlEncoded
    @POST("/api/users/update")
    public void updateUser(@Field("id") String id, @Field("image") String image,
                           @Field("thumbnail") String thumbnail , @Field("email") String email ,
                           @Field("name") String name, @Field("city") String city ,
                           @Field("birthday") String birthday, @Field("phoneNumber") String phone, Callback<String> callback);
}
