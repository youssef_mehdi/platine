package com.platine.project.platine.login.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by you on 06/01/2017.
 */

public class User implements Serializable {

    private UUID id;
    private String token;
    private String email;
    private String password;
    private String name;
    private String city;
    private String image;
    private String thumbnail;
    private int trips_participated;
    private int trips_organized;
    private String sex;
    private String birthday;
    private int reported;
    private String phone_number;

    public User(String email, String password, String name, String city,
                String sex, String birthday) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.city = city;
        this.sex = sex;
        this.birthday = birthday;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public User withId(UUID id) {
        this.id = id;
        return this;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User withEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User withPassword(String password) {
        this.password = password;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User withName(String name) {
        this.name = name;
        return this;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public User withCity(String city) {
        this.city = city;
        return this;
    }

    public int getTripsParticipated() {
        return trips_participated;
    }

    public void setTripsParticipated(int tripsParticipated) {
        this.trips_participated = tripsParticipated;
    }

    public User withTripsParticipated(int tripsParticipated) {
        this.trips_participated = tripsParticipated;
        return this;
    }

    public int getTripsOrganized() {
        return trips_organized;
    }

    public void setTripsOrganized(int tripsOrganized) {
        this.trips_organized = tripsOrganized;
    }

    public User withTripsOrganized(int tripsOrganized) {
        this.trips_organized = tripsOrganized;
        return this;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public User withSex(String sex) {
        this.sex = sex;
        return this;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public User withBirthday(String birthday) {
        this.birthday = birthday;
        return this;
    }

    public int getReported() {
        return reported;
    }

    public void setReported(int reported) {
        this.reported = reported;
    }

    public User withReported(int reported) {
        this.reported = reported;
        return this;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String convertToAdequateDate(String d){

        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date result1 = null;
        try {
            result1 = df1.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String format = formatter.format(result1);
        return format;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public int calculAge(){
        String d = convertToAdequateDate(birthday);
        long dateBirthInMills = new Date(d).getTime();
        long ageInMills = new Date().getTime() - dateBirthInMills;
        Date age = new Date(ageInMills);
        return age.getYear();
    }
}
