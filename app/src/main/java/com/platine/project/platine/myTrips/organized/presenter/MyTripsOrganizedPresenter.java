package com.platine.project.platine.myTrips.organized.presenter;

import android.content.Context;

import com.platine.project.platine.addOffer.module.Trips;
import com.platine.project.platine.commun.utils.MySharedPreferences;
import com.platine.project.platine.commun.utils.WebServiceTools;
import com.platine.project.platine.myTrips.organized.service.MyTripsOrganizedWebService;
import com.platine.project.platine.myTrips.organized.view.IMyTripsService;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by you on 21/01/2017.
 */

public class MyTripsOrganizedPresenter extends WebServiceTools{

    Context mContext;
    MyTripsOrganizedWebService iMyTripsOrganizedWebService = new RestAdapter.Builder()
            .setEndpoint(WEBSERVICELINK)
            .setLogLevel(RestAdapter.LogLevel.FULL)
            .build()
            .create(MyTripsOrganizedWebService.class);

    IMyTripsService iMyTripsService;
    public MyTripsOrganizedPresenter(IMyTripsService iMyTripsService , Context ctx) {
        this.iMyTripsService = iMyTripsService;
        mContext = ctx;
    }

    // recupere toutes les offres
    public void getTripsById(){
        String id = MySharedPreferences.getStoredId(mContext);
        iMyTripsService.loadingShow(true);
        iMyTripsOrganizedWebService.getTripsByUserId(id , new Callback<List<Trips>>() {
            @Override
            public void success(List<Trips> trips, Response response) {
                iMyTripsService.loadingShow(false);
                iMyTripsService.listTrips(trips);
                if ( trips.size() == 0){
                    iMyTripsService.list_offer_empty();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                iMyTripsService.loadingShow(false);
                iMyTripsService.loadingError("Erreur de chargement, réessayez");
            }
        });
    }
}
