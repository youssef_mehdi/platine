package com.platine.project.platine.commun.fcm;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by you on 18/01/2017.
 */

public class FcmService extends FirebaseInstanceIdService {

    String id_user; // un id user possède un token

    public FcmService() {

    }

    public FcmService(String id_user) {
        super();
        this.id_user = id_user;
    }

    FcmWebService iFcmWebService = new RestAdapter.Builder()
            .setEndpoint("https://vast-bastion-62581.herokuapp.com")
            .setLogLevel(RestAdapter.LogLevel.FULL)
            .build()
            .create(FcmWebService.class);


    // permet de rafraichir le token et l'envoyer au serveur
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        // Envoyer l'id user et son token au serveur
        iFcmWebService.sendRegistrationToServer(id_user, refreshedToken, new Callback<String>() {
            @Override
            public void success(String s, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }


}
