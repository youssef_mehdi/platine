package com.platine.project.platine.home.service;

import com.platine.project.platine.addOffer.module.Trips;
import com.platine.project.platine.login.model.User;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by you on 08/01/2017.
 */

public interface IHomeWebService {

    @GET("/api/trips")
    public void getAllTrips(Callback<List<Trips>> callback);

    @GET("/api/trips/{fromCity}/{toCity}/{dateD}/{dateA}/{priceMax}/{priceMin}")
    public void getAllTripsFindByForm(@Path("fromCity") String fromCity , @Path("toCity") String toCity,
                                    @Path("dateD") String dateBegin , @Path("dateA") String dateReturn,
                                    @Path("priceMax") int priceMax , @Path("priceMin") int priceMin,
                                    Callback<List<Trips>> callback);



}
