package com.platine.project.platine.login_register.view;

import android.view.View;

/**
 * Created by you on 06/01/2017.
 */

public interface ILoginRegisterService {

    public void goToLoginScreen();
    public void goToRegisterScreen();

}
