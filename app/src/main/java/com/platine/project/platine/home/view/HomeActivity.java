package com.platine.project.platine.home.view;

import android.os.Bundle;
import android.widget.TextView;

import com.platine.project.platine.R;
import com.platine.project.platine.commun.AbstractActivity;

import butterknife.BindView;

/**
 * Created by you on 06/01/2017.
 */

public class HomeActivity extends AbstractActivity{

    @BindView(R.id.app_name_text) TextView app_name_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //changer le nom de l'activité
        app_name_text.setText("Accueil");
        HomeFragment mHomeFragment = new HomeFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, mHomeFragment)
                .commit();
    }
    // quand on est sur l'accuiel et qu'on clique sur le bouton retour du téléphone
    // il se passe rien
    @Override
    public void onBackPressed(){

    }

}
