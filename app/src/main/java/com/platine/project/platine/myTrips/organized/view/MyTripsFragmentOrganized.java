package com.platine.project.platine.myTrips.organized.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.platine.project.platine.R;
import com.platine.project.platine.addOffer.module.Trips;
import com.platine.project.platine.home.view.HomeActivity;
import com.platine.project.platine.myTrips.organized.presenter.MyTripsOrganizedPresenter;
import com.platine.project.platine.myTrips.organized.presenter.TripOrganizedAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by you on 20/01/2017.
 */

public class MyTripsFragmentOrganized extends Fragment implements IMyTripsService {

    MyTripsOrganizedPresenter myTripsOrganizedPresenter;
    @BindView(R.id.recyclerView_organized) RecyclerView mRecyclerView;
    @BindView(R.id.empty_list_organized) TextView empty_list_organized;
    @BindView(R.id.progressBarOrganized) ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // On récupère la view du fragment
        View view = inflater.inflate(R.layout.activity_mytrips_fragment_organized,container,false);

        // On bind la view du fragment pour l'utiliser avec ButterKnife.
        ButterKnife.bind(this,view);

        myTripsOrganizedPresenter = new MyTripsOrganizedPresenter(this , getContext());
        myTripsOrganizedPresenter.getTripsById();

        return view;
    }


    @Override
    public void listTrips(List<Trips> listTrip) {
        TripOrganizedAdapter mAdapter = new TripOrganizedAdapter(listTrip);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void loadingShow(boolean bool) {
        if(bool){
            progressBar.setVisibility(View.VISIBLE);
        }
        else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void loadingError(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setPositiveButton("Fermer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(getActivity() , HomeActivity.class);
                startActivity(intent);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    public void list_offer_empty() {
        empty_list_organized.setVisibility(View.VISIBLE);
    }
}
