package com.platine.project.platine.commun.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.UUID;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by you on 16/01/2017.
 */

public class MySharedPreferences {


    // cette fonction sauvegarde id de l'utilisateur connecté à l'application
    public static void putId(Context ctx, UUID userId) {
        SharedPreferences.Editor editor = ctx.getSharedPreferences("myPref", MODE_PRIVATE).edit();
        editor.putString("idName", String.valueOf(userId));
        editor.commit();

    }

    // cette fonction permet de récupérer l'id de l'utilisateur qui est connecté à l'application
    public static String getStoredId(Context ctx) {
        SharedPreferences sp;
        sp = ctx.getSharedPreferences("myPref", Context.MODE_PRIVATE);
        String storedValue = sp.getString("idName", "null");
        return storedValue;
    }


}
