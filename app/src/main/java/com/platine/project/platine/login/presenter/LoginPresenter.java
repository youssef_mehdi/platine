package com.platine.project.platine.login.presenter;

import android.content.Context;
import android.util.Log;

import com.platine.project.platine.commun.fcm.FcmService;
import com.platine.project.platine.commun.utils.MySharedPreferences;
import com.platine.project.platine.commun.utils.WebServiceTools;
import com.platine.project.platine.login.service.ILoginWebService;
import com.platine.project.platine.login.view.ILoginService;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by you on 06/01/2017.
 */

/**
 * Presenter de l'activity login
 */
public class LoginPresenter extends WebServiceTools {

    private Context mContext;

    /**
     * Initialisation de l'appel web service
     */
    ILoginWebService iLoginWebService = new RestAdapter.Builder()
            .setEndpoint(WEBSERVICELINK)
            .setLogLevel(RestAdapter.LogLevel.FULL)
            .build()
            .create(ILoginWebService.class);

    // l'interface représentant la vue.
    private ILoginService mILoginService;

    // constructeur
    public LoginPresenter(ILoginService iLoginService, Context context) {
        mContext = context;
        mILoginService = iLoginService;
    }

    // verifier si le format de l'email rentré par l'utilisateur est valide
    public boolean verifyEmail(String email) {
        Pattern p = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher m = p.matcher(email);
        if (m.matches()) {
            return true;
        } else {
            mILoginService.mailInvalid("mail invalide , exemple@gmail.com");
            return false;
        }


    }

    // verifier si le format de password est valide : longueur > 6 et contient au moins un chiffre
    public boolean verifyPassword(String pwd) {
        Pattern p = Pattern.compile("^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$");
        Matcher m = p.matcher(pwd);
        if (pwd.length() < 6) {
            mILoginService.passwordInvalid("le mot de passe doit contenir au minimum 6 caractères");
            return false;
        }
        if (m.matches()) {
            return true;
        } else {
            mILoginService.passwordInvalid("le mot de passe doit contenir au moins une lettre, un chiffre et doit être supérieurs à 6 caractères");
            return false;
        }
    }

    // le fonction fait appel au web service
    public void connect(String mail, String password) {
        if (verifyEmail(mail) && verifyPassword(password)) {
            mILoginService.showLoading(true);
            // l'appel à la fonction loginUser qui appelle le web service
            iLoginWebService.loginUser(mail, password, new Callback<UUID>() {
                /**
                 * si l'appel web service se passe bien
                 * @param id  représente l'id renvoyé par l'appel web service
                 * @param response
                 */
                @Override
                public void success(UUID id, Response response) {
                    // on affiche plus le loader
                    mILoginService.showLoading(false);
                    // on sauvegarde l'id dans l'application
                    MySharedPreferences.putId(mContext, id);
                    // on fait appel a la fonction isLoginSuccess qui redirige l'utilisateur vers l'activity home
                    mILoginService.isLoginSuccess();

                }

                /**
                 * si l'appel web service rencontre un problème
                 * @param error représente l'erreur rencontré lors de l'appel web service
                 */
                @Override
                public void failure(RetrofitError error) {
                    mILoginService.showLoading(false);
                    Log.d("LoginPresenterrrr", "failure: " + error.getMessage());
                    if (error.getCause().toString().equals("java.lang.IllegalArgumentException: Invalid UUID: -1")) {
                        mILoginService.passwordError();
                    } else if (error.getMessage().equals("Unable to resolve host \"vast-bastion-62581.herokuapp.com\": No address associated with hostname")) {
                        //on appel la fonction loginError qui affiche une alertDialog spécifiant l'erreur
                        mILoginService.loginError("Impossible de se connecter à Internet\n\nVérifiez que vous avez activé votre connexion");
                    }
                }
            });
        }
    }


    public void fcmRegistration() {

        FcmService fcmService = new FcmService(MySharedPreferences.getStoredId(mContext));
        fcmService.onTokenRefresh();
    }

}
