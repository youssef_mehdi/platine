package com.platine.project.platine.myTrips.model;

import java.util.UUID;

/**
 * Created by you on 20/01/2017.
 */

public class Participation {

    UUID id;
    UUID id_user;
    UUID id_trip;
    int confirmed; // pour savoir si le proprietaire a accepté la demande
    String thumbnail; // l'image de l'utilisateur

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId_user() {
        return id_user;
    }

    public void setId_user(UUID id_user) {
        this.id_user = id_user;
    }

    public UUID getId_trip() {
        return id_trip;
    }

    public void setId_trip(UUID id_trip) {
        this.id_trip = id_trip;
    }

    public int getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(int confirmed) {
        this.confirmed = confirmed;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
