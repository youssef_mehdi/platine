package com.platine.project.platine.login.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.platine.project.platine.R;
import com.platine.project.platine.commun.utils.MySharedPreferences;
import com.platine.project.platine.home.view.HomeActivity;
import com.platine.project.platine.login.presenter.LoginPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by you on 13/01/2017.
 */

public class LoginFragment extends Fragment implements ILoginService {

    private static final String TAG = "LoginActivity";

    // récuperation des composants de l'activity
    @BindView(R.id.edit_text_email)
    EditText editText_mail; // le champs texte email
    @BindView(R.id.edit_text_password)
    EditText editText_password; // le champs texte password
    @BindView(R.id.progressBar)
    ProgressBar progressBar; // la bar de progression
    @BindView(R.id.passwordError)
    TextView passwordError;

    private LoginPresenter mLoginPresenter; // le presenter

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // On récupère la view du fragment
        View view = inflater.inflate(R.layout.activity_login_fragment, container, false);

        // On bind la view du fragment pour l'utiliser avec ButterKnife.
        ButterKnife.bind(this, view);

        // On initialise notre presenter
        mLoginPresenter = new LoginPresenter(this, getContext());

        return view;
    }


    @OnClick(R.id.but_login)
    public void goToHome() {
        //la fonction connect verifie l'email et le password et fait appel au web service
        mLoginPresenter.connect(editText_mail.getText().toString(), editText_password.getText().toString());
    }


    @Override
    public void showLoading(boolean bool) {
        if (bool) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void mailInvalid(String msg) {
        editText_mail.setError(msg);
    }

    @Override
    public void passwordInvalid(String msg) {
        editText_password.setError(msg);
    }

    @Override
    public void loginError(String msgError) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msgError);
        builder.setCancelable(true);
        builder.setPositiveButton("Réessayez", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void isLoginSuccess() {
        Log.d(TAG, "isLoginSuccess: " + MySharedPreferences.getStoredId(getContext()));
        mLoginPresenter.fcmRegistration();
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);
    }

    @Override
    public void passwordError() {
        passwordError.setVisibility(View.VISIBLE);
    }


}
