package com.platine.project.platine.profil.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mvc.imagepicker.ImagePicker;
import com.platine.project.platine.R;
import com.platine.project.platine.commun.utils.ImageConverterCircle;
import com.platine.project.platine.commun.utils.ImageTools;
import com.platine.project.platine.commun.utils.MySharedPreferences;
import com.platine.project.platine.login.model.User;
import com.platine.project.platine.login_register.view.LoginRegisterActivity;
import com.platine.project.platine.profil.presenter.ProfilPresenter;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by you on 12/01/2017.
 */
public class ProfilFragment extends Fragment implements IProfilService {

    private static final int PICK_IMAGE_ID = 234;

    @BindView(R.id.image_profil)
    ImageView imageProfil;
    @BindView(R.id.profil_genre)
    TextView profil_genre;
    @BindView(R.id.profil_birthCity)
    EditText profil_birthCity;
    @BindView(R.id.profil_birthday)
    EditText profil_birthDay;
    @BindView(R.id.profil_email)
    EditText profil_email;
    @BindView(R.id.profil_name)
    EditText profil_name;
    @BindView(R.id.profil_phone)
    EditText profil_phone;
    @BindView(R.id.profil_nbr_trip_organized)
    TextView profil_nbr_trip_organized;
    @BindView(R.id.profil_nbr_trip_participated)
    TextView profil_nbr_trip_participated;
    @BindView(R.id.progressBar_profil)
    ProgressBar progressBar;

    private ProfilPresenter mProfilPresenter;
    private Bitmap bitmapSelected;
    private Bitmap imageOlder; // l'image qui apparait sur le profil
    private Bitmap imageSelectedCircled;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // On récupère la view du fragment
        View view = inflater.inflate(R.layout.activity_profil_fragment, container, false);

        // On bind la view du fragment pour l'utiliser avec ButterKnife.
        ButterKnife.bind(this, view);

        // On initialise notre presenter
        mProfilPresenter = new ProfilPresenter(this, getContext());

        // idUser : l'id du profil, si on veut voir le profil de quelqun d'autre
        String idUser = (String) getActivity().getIntent().getStringExtra("idUser");
        if (idUser != null) {
            imageProfil.setEnabled(false);
            profil_genre.setEnabled(false);
            profil_birthCity.setEnabled(false);
            profil_birthDay.setEnabled(false);
            profil_email.setEnabled(false);
            profil_name.setEnabled(false);
            profil_phone.setEnabled(false);
            mProfilPresenter.getUser(idUser);
        } else {
            mProfilPresenter.getUser(MySharedPreferences.getStoredId(getContext()));
        }

        return view;
    }

    // pour choisir l'image dans la gallery du telephone ou par camera
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_IMAGE_ID:
                // verifier les données
                if (data != null) {
                    bitmapSelected = ImagePicker.getImageFromResult(getActivity(), requestCode, resultCode, data);
                    imageProfil.setImageBitmap(bitmapSelected);
                    bitmapSelected = ImageTools.resizeBitmap(bitmapSelected, true);
                    bitmapSelected = ImageConverterCircle.getCircleBitmap(bitmapSelected);

                    imageProfil.setImageBitmap(bitmapSelected);
                    break;
                }

        }
    }

    // quand on clique sur l'imageView pour choisir une image de la gallery
    @OnClick(R.id.image_profil)
    public void onPickImage(View view) {
        Intent chooseImage = ImagePicker.getPickImageIntent(getActivity(), "choisissez votre photo");
        startActivityForResult(chooseImage, PICK_IMAGE_ID);
    }

    @OnClick(R.id.but_upload_image)
    public void uploadImage() {
        String email = profil_email.getText().toString();
        String name = profil_name.getText().toString();
        String city = profil_birthCity.getText().toString();
        String birthday = profil_birthDay.getText().toString();
        String phone = profil_phone.getText().toString();

        if (bitmapSelected != null) {
            mProfilPresenter.updateImage(imageSelectedCircled, bitmapSelected , email ,
                    name , city , birthday, phone );
        }
        else if (imageOlder != null){
            mProfilPresenter.updateImage(imageSelectedCircled, imageOlder , email ,
                    name , city , birthday, phone);
        }
    }

    @OnClick(R.id.but_logout)
    public void logOut() {
        UUID id = null;
        MySharedPreferences.putId(getContext(), id);
        Intent intent = new Intent(getContext(), LoginRegisterActivity.class);
        startActivity(intent);
    }

    @Override
    public void populateFields(User user) {
        profil_genre.setText(user.getSex());
        profil_birthCity.setText(user.getCity());
        profil_birthDay.setText(user.getBirthday());
        profil_email.setText(user.getEmail());
        profil_name.setText(user.getName());
        profil_nbr_trip_organized.setText("" + user.getTripsOrganized());
        profil_nbr_trip_participated.setText("" + user.getTripsParticipated());
        profil_phone.setText(user.getPhone_number());
        if (user.getThumbnail() != "" && user.getThumbnail() != null) {
            imageOlder = ImageTools.base64ToBitmap(user.getThumbnail());
            imageProfil.setImageBitmap(ImageConverterCircle.getCircleBitmap(imageOlder));
        }
    }

    @Override
    public void failureLoadingUser(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void imageUploadSuccess(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void imageUploadFailure(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoading(boolean bool) {
        if (bool) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
}
