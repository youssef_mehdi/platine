package com.platine.project.platine.profil.view;

import com.platine.project.platine.login.model.User;

/**
 * Created by you on 12/01/2017.
 */

public interface IProfilService {

    public void populateFields(User user);
    public void failureLoadingUser(String msg);
    public void imageUploadSuccess(String msg);
    public void imageUploadFailure(String msg);
    public void showLoading(boolean bool);
}
