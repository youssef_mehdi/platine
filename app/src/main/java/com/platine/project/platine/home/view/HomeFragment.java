package com.platine.project.platine.home.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.platine.project.platine.R;
import com.platine.project.platine.addOffer.module.Trips;
import com.platine.project.platine.addOffer.view.AddOfferActivity;
import com.platine.project.platine.commun.auto_complete_text.ServicePlaceGoogle;
import com.platine.project.platine.commun.datePicker.DatePickerService;
import com.platine.project.platine.home.presenter.HomePresenter;
import com.platine.project.platine.home.presenter.TripAdapter;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by you on 14/01/2017.
 */

public class HomeFragment extends Fragment implements IHomeService {


    @BindView(R.id.autoCompleteTextViewBegin)
    EditText mAutoCompleteTextViewBegin;
    @BindView(R.id.autoCompleteTextViewDestination)
    EditText mAutoCompleteTextViewDestination;
    @BindView(R.id.date_begin_home)
    EditText date_begin_home;
    @BindView(R.id.date_return_home)
    EditText date_return_home;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.rangeSeekBar)
    RangeSeekBar rangeSeekBar;
    @BindView(R.id.list_offer_empty)
    TextView list_offer_empty;
    @BindView(R.id.expandableLayout1)
    ExpandableRelativeLayout expandableLayout1;


    HomePresenter mHomePresenter;
    TripAdapter mAdapter;
    int progressSeekBarMin;
    int progressSeekBarMax;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // On récupère la view du fragment
        View view = inflater.inflate(R.layout.activity_home_fragment, container, false);

        // On bind la view du fragment pour l'utiliser avec ButterKnife.
        ButterKnife.bind(this, view);

        //enlever le focus sur les champs ville depart et ville arrivée
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        // On initialise notre presenter
        rangeSeekBar.setRangeValues(0, 1000);
        progressSeekBarMin = 0;
        progressSeekBarMax = 1000;
        rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                //Now you have the minValue and maxValue of your RangeSeekbar
                progressSeekBarMin = minValue;
                progressSeekBarMax = maxValue;
            }
        });

        // initialisation des champs de recherche de ville
        //ServicePlaceGoogle mServicePlaceGoogle = new ServicePlaceGoogle(getActivity(), mAutoCompleteTextViewBegin, mAutoCompleteTextViewDestination);
        // initialisation des champs de date picker
        DatePickerService datePickerService = new DatePickerService(getActivity(), date_begin_home, date_return_home);

        mHomePresenter = new HomePresenter(this);
        //récuperer tout les trips du serveur
        mHomePresenter.getAllTrips();
        return view;
    }

    // afficher et cacher le formulaire
    @OnClick(R.id.expandableButton1)
    public void expandableButton1(View view) {
        expandableLayout1.toggle(); // toggle expand and collapse
    }

    // appeler quand on clique sur le bouton valider et envoie les inforamtions
    //du formulaire au web service
    @OnClick(R.id.but_home_submit)
    @Override
    public void searchTripsWithForms() {
        String fromCity = mAutoCompleteTextViewBegin.getText().toString();
        String toCity = mAutoCompleteTextViewDestination.getText().toString();
        String dateBegin = date_begin_home.getText().toString();
        String dateReturn = date_return_home.getText().toString();

        mHomePresenter.getTripsFindForms(fromCity, toCity, dateBegin, dateReturn, progressSeekBarMax, progressSeekBarMin);

    }

    // redirection vers l'activity ajouter offre
    @OnClick(R.id.but_add_my_offer)
    public void goToAddOfferActivity() {
        Intent intent = new Intent(getActivity(), AddOfferActivity.class);
        startActivity(intent);
    }


    /**
     * met a jours l'adapter avec la nouvelle liste d'offre
     *
     * @param listTrip la liste d'offre envoyé par l'appel web service
     */
    @Override
    public void listTrips(List<Trips> listTrip) {
        list_offer_empty.setVisibility(View.GONE);
        mAdapter = new TripAdapter(listTrip);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void loadingShow(boolean bool) {
        if (bool) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void loadingError(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setPositiveButton("Réessayez", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                mHomePresenter.getAllTrips();
            }
        });
        builder.setNegativeButton("Fermer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    // afficher un msg quand la liste d'offre est vide
    @Override
    public void list_offer_empty(String msg) {
        mRecyclerView.setVisibility(View.GONE);
        list_offer_empty.setText(msg);
        list_offer_empty.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume(){
        super.onResume();
        mHomePresenter.getAllTrips();
    }

}
