package com.platine.project.platine.register.service;

import com.platine.project.platine.login.model.User;

import java.util.UUID;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by you on 07/01/2017.
 */

public interface IRegisterWebService {

    @FormUrlEncoded
    @POST("/api/signup")
    public void createUser(@Field("email") String email , @Field("password") String password,
                           @Field("name") String name, @Field("city") String city,
                           @Field("sex") String sex,
                           @Field("birthday") String birthday,
                           @Field("phoneNumber") String phoneNumber, Callback<String> callback);
}
