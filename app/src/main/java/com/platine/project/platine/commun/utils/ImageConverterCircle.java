package com.platine.project.platine.commun.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

/**
 * Created by you on 17/01/2017.
 */

/**
 * cette classe permet de convertir une image carré ou rectangulaire en image circulaire
 */
public class ImageConverterCircle {

    public static Bitmap getCircleBitmap(Bitmap bitmap){
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth() , bitmap.getHeight() , Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0,0 ,bitmap.getWidth() , bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0,0,0,0);
        paint.setColor(color);
        canvas.drawCircle(bitmap.getWidth()/2 , bitmap.getHeight()/2 ,bitmap.getWidth()/2 , paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap , rect , rect , paint);

        return output;
    }
}
