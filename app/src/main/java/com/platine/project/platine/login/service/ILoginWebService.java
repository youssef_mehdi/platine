package com.platine.project.platine.login.service;

import com.platine.project.platine.login.model.User;

import java.util.UUID;

import retrofit.Callback;
import retrofit.http.*;

/**
 * Created by you on 06/01/2017.
 */

public interface ILoginWebService {


    // appel asynchrone
    @GET("/api/login/{email}/{password}")
    public void loginUser(@Path("email") String email , @Path("password") String password , Callback<UUID> callback);

    //appel synchrone
    @GET("/api/login/{email}/{password}")
    public UUID loginUser(@Path("email") String email , @Path("password") String password);
}
