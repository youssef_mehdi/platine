package com.platine.project.platine.login.view;

import android.os.Bundle;
import android.view.View;

import com.platine.project.platine.R;
import com.platine.project.platine.commun.AbstractActivity;

/**
 * Created by you on 06/01/2017.
 */
public class LoginActivity extends AbstractActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoginFragment mLoginFragment = new LoginFragment();

        //desactiver la toolbar
        mToolbar.setVisibility(View.GONE);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, mLoginFragment)
                .commit();
    }
}
