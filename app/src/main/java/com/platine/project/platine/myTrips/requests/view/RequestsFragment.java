package com.platine.project.platine.myTrips.requests.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.platine.project.platine.R;
import com.platine.project.platine.myTrips.requests.model.ResponseRequest;
import com.platine.project.platine.myTrips.requests.presenter.RequestPresenter;
import com.platine.project.platine.myTrips.requests.presenter.TripRequestAdapter;
import com.platine.project.platine.myTrips.view.MyTripsActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by you on 21/01/2017.
 */

public class RequestsFragment extends Fragment implements IRequestService {

    RequestPresenter mRequestPresenter;
    @BindView(R.id.request_progressBar)
    ProgressBar progressBar;
    @BindView(R.id.request_recyclerView)
    RecyclerView request_recyclerView; // recycler view des demandes acceptées
    @BindView(R.id.request_recyclerView_accepted)
    RecyclerView request_recyclerView_accepted; // recycler view des demandes en cours
    @BindView(R.id.request_empty)
    TextView request_empty;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // On récupère la view du fragment
        View view = inflater.inflate(R.layout.activity_requests_fragment, container, false);

        // On bind la view du fragment pour l'utiliser avec ButterKnife.
        ButterKnife.bind(this, view);

        // recevoir l'id du trip qu'on a séléctionné
        String tripSelectedId = (String) getActivity().getIntent().getStringExtra("tripSelectedId");

        String tripSelectedIdFromParticipated = (String) getActivity().getIntent().getStringExtra("tripSelectedIdFromParticipated");

        //initialisation du presenter
        mRequestPresenter = new RequestPresenter(this);

        // ça veut dire qu'on vient de l'activity participated,
        // on affiche tout les users qui participent a ce voyage
        if (tripSelectedIdFromParticipated != null) {
            mRequestPresenter.getUsersAccepted(tripSelectedIdFromParticipated);
        }
        mRequestPresenter.getRequest(tripSelectedId);

        return view;
    }

    //affichage du loader
    @Override
    public void showLoading(boolean bool) {
        if (bool) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * met a jours l'adapter avec la liste requestsList
     *
     * @param requestsList
     */
    @Override
    public void listUsers(List<ResponseRequest> requestsList) {
        List<ResponseRequest> listDemandeEnCours = new ArrayList<>(); // contient les demandes en cours
        List<ResponseRequest> listDemandeAcceptee = new ArrayList<>(); // contient les demandes acceptées

        for(ResponseRequest r : requestsList){
            if (r.getConfirmed() == 0){
                listDemandeEnCours.add(r);
            }
            else {
                listDemandeAcceptee.add(r);
            }
        }
        // 2 recucler view : le premier contient les demandes accepté
        // le deuxieme contient les demandes en cours
        TripRequestAdapter tripRequestAdapter = new TripRequestAdapter(listDemandeAcceptee, getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        request_recyclerView_accepted.setLayoutManager(mLayoutManager);
        request_recyclerView_accepted.setItemAnimator(new DefaultItemAnimator());
        request_recyclerView_accepted.setAdapter(tripRequestAdapter);

        TripRequestAdapter tripRequestEnCoursAdapter = new TripRequestAdapter(listDemandeEnCours, getContext());
        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getContext());
        request_recyclerView.setLayoutManager(mLayoutManager2);
        request_recyclerView.setItemAnimator(new DefaultItemAnimator());
        request_recyclerView.setAdapter(tripRequestEnCoursAdapter);
    }

    // aucune demande a afficher
    @Override
    public void list_request_empty(){
        request_empty.setVisibility(View.VISIBLE);
    }

    @Override
    public void addedSuccessfully(){
        Intent intent = new Intent(getActivity() , RequestsActivity.class);
        startActivity(intent);
    }

    @Override
    public void addedFailed(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Une erreur est survenue lors d'ajout/refus de l'utilisateur");
        builder.setCancelable(true);
        builder.setPositiveButton("Fermer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(getActivity() , MyTripsActivity.class);
                startActivity(intent);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }

    @Override
    public void errorLoading() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Une erreur est survenue");
        builder.setCancelable(true);
        builder.setPositiveButton("Fermer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(getActivity() , MyTripsActivity.class);
                startActivity(intent);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}
