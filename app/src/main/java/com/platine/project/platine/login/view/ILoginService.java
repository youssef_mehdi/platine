package com.platine.project.platine.login.view;

import java.util.UUID;

/**
 * Created by you on 06/01/2017.
 */

/**
 * interface représentant les différentes actions de l'activity Login
 */

public interface ILoginService {

    // montre le loader quand bool =true et le cache sinon
    public void showLoading(boolean bool);
    // montre le message d'erreur msg sur le champs login quand celui-ci est faux
    public void mailInvalid( String msg);
    // montre le message d'erreur msg sur le champs password quand celui-ci est faux
    public void passwordInvalid(String msg);
    // montre une alertDialog quand l'appel au web service ne fonctionne pas
    public void loginError(String msgError);
    // redirection vers l'activity home
    public void isLoginSuccess();
    public void passwordError();
}
