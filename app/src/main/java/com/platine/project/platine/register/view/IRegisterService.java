package com.platine.project.platine.register.view;

import java.util.UUID;

/**
 * Created by you on 06/01/2017.
 */

public interface IRegisterService {

    //cette fonction est appelé quand on clique sur le bouton enregistrer
    public void createUser();
    // affiche l'erreur sur le champs texte email
    public void emailInvalid(String msg);
    // affiche l'erreur sur le champs texte password
    public void passwordInvalid(String msg);
    // affiche l'erreur sur le champs mot de passe de confirmation
    public void confirmPasswordInvalid(String msg);
    // affiche le loader si bool = true,  le cache sinon
    public void showLoading(boolean bool);
    // redirection vers home
    public void registrationSuccess();
    // afficher une alert avec un message d'erreur
    public void registrationFailed(String msg);
}
