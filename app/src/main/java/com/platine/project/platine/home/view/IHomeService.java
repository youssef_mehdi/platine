package com.platine.project.platine.home.view;

import com.platine.project.platine.addOffer.module.Trips;
import com.platine.project.platine.login.model.User;

import java.util.List;

/**
 * Created by you on 08/01/2017.
 */

public interface IHomeService {

    public void searchTripsWithForms();
    public void listTrips(List<Trips> listTrip);
    public void loadingShow(boolean bool);
    public void loadingError(String msg);
    public void list_offer_empty(String msg);
}
