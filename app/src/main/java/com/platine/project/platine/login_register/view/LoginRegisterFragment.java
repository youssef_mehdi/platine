package com.platine.project.platine.login_register.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.platine.project.platine.R;
import com.platine.project.platine.login.view.LoginActivity;
import com.platine.project.platine.register.view.RegisterActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by youssef on 13/01/2017.
 */

/**
 * fragment de l'activity qui permet à l'utilisateur de choisir entre : s'authentifier et s'enregistrer
 */
public class LoginRegisterFragment extends Fragment implements ILoginRegisterService {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // On récupère la view du fragment
        View view = inflater.inflate(R.layout.activity_user_choose_fragment, container, false);

        // On bind la view du fragment pour l'utiliser avec ButterKnife.
        ButterKnife.bind(this, view);

        return view;
    }

    /**
     * quand on clique sur le bouton s'enregistrer, on redirige l'utilisateur vers
     * l'écran pour s'enregistrer
     */
    @Override
    @OnClick(R.id.but_register)
    public void goToLoginScreen() {
        Intent intent = new Intent(getActivity(), RegisterActivity.class);
        startActivity(intent);
    }

    /**
     * Redirection vers l'ecran pour s'authentifier
     */
    @OnClick(R.id.but_connexion)
    @Override
    public void goToRegisterScreen() {
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);

    }

}
