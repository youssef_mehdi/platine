package com.platine.project.platine.addOffer.view;

/**
 * Created by you on 08/01/2017.
 */

public interface IAddOfferService {

    public void createOffer();
    public void showLoading(boolean bool);
    public void addOfferSuccess(String msg);
    public void addOfferFailed(String msg);

    public void fieldFromCityIsEmpty(String msgErr);
    public void fieldToCityIsEmpty(String msgErr);
    public void fieldDateDebutIsEmpty(String msgErr);
    public void fieldDateReturnIsEmpty(String msgErr);
    public void fieldFromAirportIsEmpty(String msgErr);
    public void fieldToAirportIsEmpty(String msgErr);
    public void fieldPriceFlightIsEmpty(String msgErr);
    public void fieldNbrPlaceIsEmpty(String msgErr);
    public void fieldPriceLodgmentIsEmpty(String msgError);
    public void fieldNbrNightIsEmpty(String msgErr);






}
