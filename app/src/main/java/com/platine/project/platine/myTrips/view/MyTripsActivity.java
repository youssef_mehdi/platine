package com.platine.project.platine.myTrips.view;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.platine.project.platine.R;
import com.platine.project.platine.commun.AbstractActivity;
import com.platine.project.platine.myTrips.organized.PagerAdapter;
import com.platine.project.platine.myTrips.organized.view.MyTripsFragmentOrganized;
import com.platine.project.platine.myTrips.participated.view.ParticipatedFragment;

import butterknife.BindView;

/**
 * Created by you on 20/01/2017.
 */

public class MyTripsActivity extends AbstractActivity {

    @BindView(R.id.app_name_text) TextView app_name_text;
    @BindView(R.id.tab_layout) TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabLayout.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.VISIBLE);
        app_name_text.setText("Mes voyages");

        MyTripsFragmentOrganized mTripsFragment = new MyTripsFragmentOrganized();
        ParticipatedFragment myTripsFragmentParticipated = new ParticipatedFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, mTripsFragment)
                .add(R.id.fragment_container, myTripsFragmentParticipated)
                .commit();

        // revenir à l'activity parente
        setSupportActionBar(mToolbar);
        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        tabLayout.addTab(tabLayout.newTab().setText("Organisé"));
        tabLayout.addTab(tabLayout.newTab().setText("Participé"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }





}
