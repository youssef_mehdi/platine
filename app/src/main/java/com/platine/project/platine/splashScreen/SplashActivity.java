package com.platine.project.platine.splashScreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.platine.project.platine.commun.utils.MySharedPreferences;
import com.platine.project.platine.home.view.HomeActivity;
import com.platine.project.platine.login_register.view.LoginRegisterActivity;

/**
 * Created by you on 24/01/2017.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(MySharedPreferences.getStoredId(this) != null && !(MySharedPreferences.getStoredId(this).equals("null")) ){
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            Intent intent = new Intent(this, LoginRegisterActivity.class);
            startActivity(intent);
            finish();
        }
    }
}


