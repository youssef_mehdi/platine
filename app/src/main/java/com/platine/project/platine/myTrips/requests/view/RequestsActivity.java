package com.platine.project.platine.myTrips.requests.view;

import android.os.Bundle;
import android.widget.TextView;

import com.platine.project.platine.R;
import com.platine.project.platine.commun.AbstractActivity;

import butterknife.BindView;

/**
 * Created by you on 21/01/2017.
 */

public class RequestsActivity extends AbstractActivity {

    @BindView(R.id.app_name_text) TextView app_name_text;// le nom de l'activité

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app_name_text.setText("Participants");
        // initialisation du fragment
        RequestsFragment mRequestsFragment = new RequestsFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, mRequestsFragment)
                .commit();

        // revenir à l'activity parente
        setSupportActionBar(mToolbar);
        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

    }

}
