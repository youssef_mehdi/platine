package com.platine.project.platine.addOffer.view;

import android.os.Bundle;
import android.widget.TextView;

import com.platine.project.platine.R;
import com.platine.project.platine.commun.AbstractActivity;

import butterknife.BindView;

/**
 * Created by you on 08/01/2017.
 */


public class AddOfferActivity extends AbstractActivity {

    @BindView(R.id.app_name_text)
    TextView app_name_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //changer le nom de l'acitivity
        app_name_text.setText("Ajouter une offre");
        AddOfferFragment mRegisterFragment = new AddOfferFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, mRegisterFragment)
                .commit();

        // revenir à l'activity parente
        setSupportActionBar(mToolbar);
        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
    }


}
