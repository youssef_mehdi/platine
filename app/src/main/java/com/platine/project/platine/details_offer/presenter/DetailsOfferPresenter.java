package com.platine.project.platine.details_offer.presenter;

import android.content.Context;

import com.platine.project.platine.commun.utils.MySharedPreferences;
import com.platine.project.platine.commun.utils.WebServiceTools;
import com.platine.project.platine.details_offer.service.DetailsOfferWebService;
import com.platine.project.platine.details_offer.view.IDetailsOfferService;

import java.util.UUID;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by you on 09/01/2017.
 */

public class DetailsOfferPresenter extends WebServiceTools {

    IDetailsOfferService mIDetailsOfferService; // les services de la vue
    Context mContext;

    //initialisation du web service
    DetailsOfferWebService iDetailsOfferWebService = new RestAdapter.Builder()
            .setEndpoint(WEBSERVICELINK)
            .setLogLevel(RestAdapter.LogLevel.FULL)
            .build()
            .create(DetailsOfferWebService.class);

    /**
     * constructeur
     * @param iDetailsOfferService la vue
     * @param context le contexte
     */
    public DetailsOfferPresenter(IDetailsOfferService iDetailsOfferService, Context context) {
        mIDetailsOfferService = iDetailsOfferService;
        mContext = context;
    }

    /**
     * envoie au serveur l'id user et l'id trip, reçoit "1" si tout est bien envoyé "-1" sinon
     * @param idTrip id trip laquelle on veut rejoindre
     */
    public void sendRequest(UUID idTrip) {
        String idUser = MySharedPreferences.getStoredId(mContext);
        iDetailsOfferWebService.sendJoinRequest(idUser, idTrip, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                if (s.equals("1")) {
                    mIDetailsOfferService.requestParticipationSendSuccessfully("la demande de participation a bien été envoyée");
                } else {
                    mIDetailsOfferService.requestParticipationSendFailure("Vous avez déjà envoyé une demande pour ce voyage");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                mIDetailsOfferService.serverError("le serveur rencontre des problèmes, réessayez plus tard");
            }
        });
    }
}

