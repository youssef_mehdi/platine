package com.platine.project.platine.myTrips.participated.service;

import com.platine.project.platine.addOffer.module.Trips;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by you on 23/01/2017.
 */

public interface IParticipatedWebService {

    @GET("/api/trips/getTripsParticipatedByUserId/{idUser}")
    public void getTripsParticipatedByUserId(@Path("idUser") String idUser , Callback<List<Trips>> callback);
}
