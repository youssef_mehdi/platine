package com.platine.project.platine.myTrips.requests.view;

import com.platine.project.platine.myTrips.requests.model.ResponseRequest;

import java.util.List;

/**
 * Created by you on 22/01/2017.
 */

public interface IRequestService {

    public void showLoading(boolean bool);
    public void listUsers(List<ResponseRequest> users);
    public void list_request_empty();
    public void addedSuccessfully();
    public void addedFailed();
    public void errorLoading();
}
