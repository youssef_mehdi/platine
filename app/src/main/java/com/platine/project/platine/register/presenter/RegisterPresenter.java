package com.platine.project.platine.register.presenter;

import android.content.Context;
import android.content.SharedPreferences;

import com.platine.project.platine.commun.fcm.FcmService;
import com.platine.project.platine.commun.utils.MySharedPreferences;
import com.platine.project.platine.commun.utils.WebServiceTools;
import com.platine.project.platine.register.service.IRegisterWebService;
import com.platine.project.platine.register.view.IRegisterService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by you on 06/01/2017.
 */

public class RegisterPresenter extends WebServiceTools {

    IRegisterService mIRegisterService; // la vue
    Context mContext;

    //initialisation du web service
    IRegisterWebService mWebService = new RestAdapter.Builder()
            .setEndpoint(WEBSERVICELINK)
            .setLogLevel(RestAdapter.LogLevel.FULL)
            .build()
            .create(IRegisterWebService.class);


    public RegisterPresenter(Context context, IRegisterService iRegisterService) {
        mContext = context;
        mIRegisterService = iRegisterService;
    }

    // controle de surface sur le champs email
    public boolean verifyEmail(String email) {
        Pattern p = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher m = p.matcher(email);
        if (m.matches()) {
            return true;
        } else {
            mIRegisterService.emailInvalid("mail doit être sous forme: exemple@gmail.com");
            return false;
        }
    }

    // controle de surface sur le champs mot de passe
    public boolean verifyPassword(String pwd, String pwdConfirm) {
        Pattern p = Pattern.compile("^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$");
        Matcher m = p.matcher(pwd);
        if (pwd.length() < 6) {
            mIRegisterService.passwordInvalid("le mot de passe doit contenir au minimum 6 caractères");
            return false;
        }
        if (m.matches()) {
            return true;
        }
        if (pwd.equals(pwdConfirm)) {
            return true;
        }
        if (!(pwd.equals(pwdConfirm))) {
            mIRegisterService.confirmPasswordInvalid("le mot de passe de confirmation ne correspond pas au mot de passe");
            return false;
        } else {
            mIRegisterService.passwordInvalid("le mot de passe doit contenir au moins une lettre, un chiffre et doit être supérieurs à 6 caractères");
            return false;
        }
    }

    // cette fonction fait appel au web service avec la méthode POST
    public void register(String name, String email, String dateBirth, String city, String sex,String phoneNumber, String pwd, String pwdConfirm) {
        if (verifyEmail(email) && verifyPassword(pwd, pwdConfirm)) {
            mIRegisterService.showLoading(true);
            mWebService.createUser(email, pwd, name, city, sex, dateBirth, phoneNumber, new Callback<String>() {
                @Override
                public void success(String id, Response response) {
                    mIRegisterService.showLoading(false);
                    if (!(id.equals("-1"))) {
                        putId(mContext, id);
                        mIRegisterService.registrationSuccess();
                    } else {
                        mIRegisterService.registrationFailed("Ce mail existe déjà!");
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    mIRegisterService.showLoading(false);
                    mIRegisterService.registrationFailed("Impossible de se connecter à Internet\n\nVérifiez que vous avez activé votre connexion");
                }
            });
        }
    }


    public void fcmRegistration() {

        FcmService fcmService = new FcmService(MySharedPreferences.getStoredId(mContext));
        fcmService.onTokenRefresh();
    }


    // cette fonction sauvegarde id de l'utilisateur
    public void putId(Context ctx, String userId) {
        SharedPreferences.Editor editor = ctx.getSharedPreferences("myPref", MODE_PRIVATE).edit();
        editor.putString("idName", userId);
        editor.commit();

    }
}
