package com.platine.project.platine.commun.auto_complete_text;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

/**
 * Created by you on 07/01/2017.
 */

public class ServicePlaceGoogle implements GoogleApiClient.OnConnectionFailedListener {

    /**
     * GoogleApiClient wraps our service connection to Google Play Services and provides access
     * to the user's sign in state as well as the Google's APIs.
     */
    protected GoogleApiClient mGoogleApiClient;

    private PlaceAutocompleteAdapter mAdapter;

    private AutoCompleteTextView mAutocompleteViewDepart;
    private AutoCompleteTextView mAutocompleteViewDestination;
    private AutoCompleteTextView mAutocompleteViewAirportDepart;
    private AutoCompleteTextView mAutocompleteViewAirportDestination;


    Context mFragmentActivity;

    // constructeur 1
    public ServicePlaceGoogle(Context fragmentActivity , AutoCompleteTextView autocompleteViewDepart ,
                              AutoCompleteTextView autocompleteViewDestination ,
                              AutoCompleteTextView  autocompleteViewAirportDepart ,
                              AutoCompleteTextView  autocompleteViewAirportDestination){

        // Construct a GoogleApiClient for the {@link Places#GEO_DATA_API} using AutoManage
        // functionality, which automatically sets up the API client to handle Activity lifecycle
        // events. If your activity does not extend FragmentActivity, make sure to call connect()
        // and disconnect() explicitly.
        mGoogleApiClient = new GoogleApiClient.Builder(fragmentActivity)
                .enableAutoManage((FragmentActivity) fragmentActivity, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();

        mAutocompleteViewDepart = autocompleteViewDepart;
        mAutocompleteViewAirportDepart = autocompleteViewAirportDepart;
        mAutocompleteViewAirportDestination = autocompleteViewAirportDestination;
        mAutocompleteViewDestination = autocompleteViewDestination;

        mFragmentActivity = fragmentActivity;

        addListener();
        setAdapter();
    }

    //constructeur 2
    public ServicePlaceGoogle(Context fragmentActivity , AutoCompleteTextView autocompleteViewDepart ,
                              AutoCompleteTextView autocompleteViewDestination){
        // Construct a GoogleApiClient for the {@link Places#GEO_DATA_API} using AutoManage
        // functionality, which automatically sets up the API client to handle Activity lifecycle
        // events. If your activity does not extend FragmentActivity, make sure to call connect()
        // and disconnect() explicitly.
        mGoogleApiClient = new GoogleApiClient.Builder(fragmentActivity)
                .enableAutoManage((FragmentActivity) fragmentActivity, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();

        mAutocompleteViewDepart = autocompleteViewDepart;
        if (autocompleteViewDestination != null)
            mAutocompleteViewDestination = autocompleteViewDestination;
        mFragmentActivity = fragmentActivity;

        addListener();
        setAdapter();
    }

    public void addListener(){
        mAutocompleteViewDepart.setOnItemClickListener(mAutocompleteClickListener);

        if(mAutocompleteViewDestination != null)
            mAutocompleteViewDestination.setOnItemClickListener(mAutocompleteClickListener);

        if( mAutocompleteViewAirportDepart != null)
            mAutocompleteViewAirportDepart.setOnItemClickListener(mAutocompleteClickListener);

        if(mAutocompleteViewAirportDestination != null)
            mAutocompleteViewAirportDestination.setOnItemClickListener(mAutocompleteClickListener);
    }

    public void setAdapter(){

        // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
        // the entire world.
        mAdapter = new PlaceAutocompleteAdapter(mFragmentActivity, mGoogleApiClient, null);
        mAutocompleteViewDepart.setAdapter(mAdapter);

        if (mAutocompleteViewDestination != null)
            mAutocompleteViewDestination.setAdapter(mAdapter);

        if (mAutocompleteViewAirportDepart != null)
            mAutocompleteViewAirportDepart.setAdapter(mAdapter);

        if(mAutocompleteViewAirportDestination != null)
            mAutocompleteViewAirportDestination.setAdapter(mAdapter);
    }


    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(com.google.android.gms.common.api.GoogleApiClient,
     * String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

        }
    };

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                places.release();
                return;
            }
            places.release();
        }
    };


    /**
     * Called when the Activity could not connect to Google Play services and the auto manager
     * could resolve the error automatically.
     * In this case the API is not available and notify the user.
     *
     * @param connectionResult can be inspected to determine the cause of the failure
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // TODO(Developer): Check error code and notify the user of error state and resolution.
        Toast.makeText(mFragmentActivity,
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }

}
