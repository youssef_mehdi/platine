package com.platine.project.platine.home.presenter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.platine.project.platine.R;
import com.platine.project.platine.commun.utils.DateTools;
import com.platine.project.platine.commun.utils.ImageConverterCircle;
import com.platine.project.platine.commun.utils.ImageTools;
import com.platine.project.platine.details_offer.view.DetailsOfferActivity;
import com.platine.project.platine.addOffer.module.Trips;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by you on 09/01/2017.
 */

public class TripAdapter extends RecyclerView.Adapter<TripAdapter.MyViewHolder> {

    private List<Trips> tripsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        // récuperation des composants utilisé pour chaque item de la recyclerview
        @BindView(R.id.depart_recycler)  TextView depart_recycler; // ville de depart
        @BindView(R.id.arrival_recycler) TextView arrival_recycler; // ville d'arrivee
        @BindView(R.id.date_depart_recycler) TextView date_depart_recycler; // date de depart
        @BindView(R.id.date_arrive_recycler) TextView date_arrive_recycler; // date d'arrivee
        @BindView(R.id.recycler_nbr_place) TextView recycler_nbr_places; // nombre de place restante
        @BindView(R.id.recycler_offre_price) TextView price; // prix de l'offre
        @BindView(R.id.imageButtonProfileHome) ImageView imageButtonProfileHome; // la photo de l'utilisateur qui a posté l'offre
        @BindView(R.id.textView_home_name_user_trip) TextView textView_home_name_user_trip; // le champs nom du proprietaire de l'offre

        public MyViewHolder(final View view) {
            super(view);
            ButterKnife.bind(this, view);
            // un listener quand on clique sur un item de la recyclerView
            // nous renvoie dans l'activity detail de l'offre selectionnée
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Intent intent = new Intent(v.getContext(), DetailsOfferActivity.class);
                    intent.putExtra("tripSelected", tripsList.get(position));
                    view.getContext().startActivity(intent);

                }
            });
        }
    }


    public TripAdapter(List<Trips> tripsList) {
        this.tripsList = tripsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.offer_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    // remplir les champs d'un item de la recyclerView
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){
        Trips trip = tripsList.get(position);
        holder.depart_recycler.setText(trip.getFrom_city());
        holder.arrival_recycler.setText(trip.getTo_city());
        holder.date_depart_recycler.setText(DateTools.convertToAdequateDate(trip.getDate_d()));
        holder.date_arrive_recycler.setText(DateTools.convertToAdequateDate(trip.getDate_a()));
        holder.recycler_nbr_places.setText(String.valueOf(trip.getNbr_places() - trip.getNbr_participants()));
        holder.price.setText(String.valueOf(trip.getPrice()) +"€");
        holder.textView_home_name_user_trip.setText(trip.getName_user());
        if (trip.getThumbnail() != null){
            Bitmap bitmap = ImageTools.base64ToBitmap(trip.getThumbnail());
            bitmap = ImageTools.resizeBitmap(bitmap , true);
            bitmap = ImageConverterCircle.getCircleBitmap(bitmap);
            holder.imageButtonProfileHome.setImageBitmap(bitmap);
        }

    }

    @Override
    public int getItemCount() {
        return tripsList.size();
    }
}
