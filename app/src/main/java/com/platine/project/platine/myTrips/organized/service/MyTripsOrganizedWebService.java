package com.platine.project.platine.myTrips.organized.service;

import com.platine.project.platine.addOffer.module.Trips;
import com.platine.project.platine.login.model.User;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by you on 21/01/2017.
 */

public interface MyTripsOrganizedWebService {


    @GET("/api/trips/getByUserId/{id}")
    public void getTripsByUserId(@Path("id") String id , Callback<List<Trips>> callback);

}
