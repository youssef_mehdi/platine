package com.platine.project.platine.details_offer.service;

import com.platine.project.platine.commun.utils.WebServiceTools;

import java.util.UUID;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by you on 18/01/2017.
 */

public interface DetailsOfferWebService {

    @FormUrlEncoded
    @POST("/api/participation/create")
    public void sendJoinRequest(@Field("idUser") String idUser ,@Field("idTrip") UUID idTrip ,Callback<String> callback);

}
