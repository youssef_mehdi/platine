package com.platine.project.platine.myTrips.organized.presenter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.platine.project.platine.R;
import com.platine.project.platine.addOffer.module.Trips;
import com.platine.project.platine.commun.utils.DateTools;
import com.platine.project.platine.details_offer.view.DetailsOfferActivity;
import com.platine.project.platine.myTrips.requests.view.RequestsActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by you on 21/01/2017.
 */

public class TripOrganizedAdapter extends RecyclerView.Adapter<TripOrganizedAdapter.MyViewHolder> {

    private List<Trips> tripsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        // récuperation des composants utilisé pour chaque item de la recyclerview
        @BindView(R.id.organizd_city_depart) TextView depart_recycler; // ville de depart
        @BindView(R.id.organizded_city_destination) TextView arrival_recycler; // ville d'arrivee
        @BindView(R.id.organized_date_depart) TextView date_depart_recycler; // date de depart
        @BindView(R.id.organized_date_arrival) TextView date_arrive_recycler; // date d'arrivee
        @BindView(R.id.organized_price_offer) TextView price; // prix de l'offre

        public MyViewHolder(final View view) {
            super(view);
            ButterKnife.bind(this, view);
            // un listener quand on clique sur un item de la recyclerView
            // nous renvoie dans l'activity detail de l'offre selectionnée
            view.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Intent intent = new Intent(v.getContext(), RequestsActivity.class);
                    intent.putExtra("tripSelectedId",""+tripsList.get(position).getId_trip());
                    view.getContext().startActivity(intent);
                }
            });
        }
    }


    public TripOrganizedAdapter(List<Trips> tripsList) {
        this.tripsList = tripsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_trip_organizd_row , parent, false);

        return new MyViewHolder(itemView);
    }

    // remplir les champs d'un item de la recyclerView
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){
        Trips trip = tripsList.get(position);
        holder.depart_recycler.setText(trip.getFrom_city());
        holder.arrival_recycler.setText(trip.getTo_city());
        holder.date_depart_recycler.setText(DateTools.convertToAdequateDate(trip.getDate_d()));
        holder.date_arrive_recycler.setText(DateTools.convertToAdequateDate(trip.getDate_a()));
        holder.price.setText(String.valueOf(trip.getPrice())+" €");
    }

    @Override
    public int getItemCount() {
        return tripsList.size();
    }
}

