package com.platine.project.platine.myTrips.participated.view;

import com.platine.project.platine.addOffer.module.Trips;

import java.util.List;

/**
 * Created by you on 23/01/2017.
 */

public interface IParticipatedService {

    public void listTrips(List<Trips> listTrip);
    public void loadingShow(boolean bool);
    public void loadingError(String msg);
    public void list_offer_empty();
}
