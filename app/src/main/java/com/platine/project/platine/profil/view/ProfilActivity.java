package com.platine.project.platine.profil.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mvc.imagepicker.ImagePicker;
import com.platine.project.platine.R;
import com.platine.project.platine.commun.AbstractActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by you on 12/01/2017.
 */

public class ProfilActivity extends AbstractActivity {

    @BindView(R.id.app_name_text)
    TextView app_name_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app_name_text.setText("Mon profil");
        ProfilFragment mProfilFragment = new ProfilFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, mProfilFragment)
                .commit();

        // revenir à l'activity parente
        setSupportActionBar(mToolbar);
        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

    }
}
