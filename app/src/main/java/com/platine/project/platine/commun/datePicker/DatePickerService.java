package com.platine.project.platine.commun.datePicker;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Created by you on 07/01/2017.
 */

public class DatePickerService {

    Calendar mCalendar; // permet de recuperer le mois, année, jour d'une date
    int day, year, month; // permettrons de sauvegarder la date du jour courant
    EditText mEditTextDateNaissance;  // le champs de la date picker correpondant à la date de naissance
    EditText mEditTextDate; // le champs de la date picker correpondant à la date de départ
    EditText mEditTextDateRetour; // le champs de la date picker correpondant à la date de retour
    Context mCtx;

    //constructeur 1
    public DatePickerService(Context ctx, EditText editTextDate, EditText editTextDateRetour) {

        mCtx = ctx;
        mEditTextDate = editTextDate;
        mEditTextDateRetour = editTextDateRetour;

        mCalendar = Calendar.getInstance();
        day = mCalendar.get(Calendar.DAY_OF_MONTH);
        year = mCalendar.get(Calendar.YEAR);
        month = mCalendar.get(Calendar.MONTH);

        mEditTextDate.setOnTouchListener(mClickListener);
        mEditTextDateRetour.setOnTouchListener(mClickListener);
    }

    //constructeur 2
    public DatePickerService(Context ctx, EditText editTextDateNaissace) {

        mCtx = ctx;
        mEditTextDateNaissance = editTextDateNaissace;

        mCalendar = Calendar.getInstance();
        day = mCalendar.get(Calendar.DAY_OF_MONTH);
        year = mCalendar.get(Calendar.YEAR);
        month = mCalendar.get(Calendar.MONTH);

        mEditTextDateNaissance.setOnTouchListener(mClickListener);

    }

    View.OnTouchListener mClickListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                if (v != null && mEditTextDateNaissance != null && v.getId() == mEditTextDateNaissance.getId()) {
                    dateDialogDateNaissance(mEditTextDateNaissance);
                } else if (v.getId() == mEditTextDate.getId()) {
                    dateDialog(mEditTextDate);
                } else {
                    dateDialog(mEditTextDateRetour);
                }
            }
            return false;
        }
    };

    public void dateDialog(final EditText date) {

        final DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int true_month = month + 1;
                date.setText(true_month + "-" + dayOfMonth + "-" + year);
            }
        };
        //initialiser la date picker a la date d'aujourd'hui
        DatePickerDialog dpDialog = new DatePickerDialog(mCtx, listener, year, month, day);
        // afficher les dates futurs
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpDialog.show();
    }

    public void dateDialogDateNaissance(final EditText date) {
        final DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int true_month = month + 1;
                date.setText(dayOfMonth + "-" + true_month + "-" + year);
            }
        };
        //initialiser la date picker a la date d'aujourd'hui
        DatePickerDialog dpDialog = new DatePickerDialog(mCtx, listener, year, month, day);
        // afficher que les dates passées
        dpDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        dpDialog.show();
    }
}
